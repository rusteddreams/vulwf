#pragma once

//              Copyright Ryhor Spivak 2016.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include <malloc.h>

#ifndef alloca
#define alloca _alloca
#endif

#include <cstdint>
#include <cassert>
#include <utility>
#include <vector>
#include <string>
#include <algorithm>
#include <array>
#include <tuple>
#include <boost/optional.hpp>

#if !defined VULWF_USE_DEBUG_REPORTING && !defined NDEBUG
#define VULWF_USE_DEBUG_REPORTING
#endif

#ifdef VULWF_USE_DEBUG_REPORTING
#include <iostream>
#endif

#ifndef VULWF_THROW
#include <stdexcept>
#include <boost/throw_exception.hpp>
#define VULWF_THROW(x) BOOST_THROW_EXCEPTION(std::runtime_error(x))
#endif

#ifndef VULWF_MAX_SWAP_IMAGES
#define VULWF_MAX_SWAP_IMAGES 4
#endif

#ifndef VULWF_WIN32_WCNAME
#define VULWF_WIN32_WCNAME L"VULWFWC"
#endif

#ifndef _WIN32_WINNT
#define _WIN32_WINNT 0x0500
#endif

#define WIN32_LEAN_AND_MEAN

#define NOGDICAPMASKS     // CC_*, LC_*, PC_*, CP_*, TC_*, RC_
//#define NOVIRTUALKEYCODES // VK_*
//#define NOWINMESSAGES     // WM_*, EM_*, LB_*, CB_*
//#define NOWINSTYLES       // WS_*, CS_*, ES_*, LBS_*, SBS_*, CBS_*
//#define NOSYSMETRICS      // SM_*
//#define NOMENUS           // MF_*
//#define NOICONS           // IDI_*
//#define NOKEYSTATES       // MK_*
//#define NOSYSCOMMANDS     // SC_*
#define NORASTEROPS       // Binary and Tertiary raster ops
//#define NOSHOWWINDOW      // SW_*
#define NOATOM            // Atom Manager routines
//#define NOCLIPBOARD       // Clipboard routines
#define NOCOLOR           // Screen colors
//#define NOCTLMGR          // Control and Dialog routines
#define NODRAWTEXT        // DrawText() and DT_*
//#define NOGDI             // All GDI defines and routines
#define NOKERNEL          // All KERNEL defines and routines
//#define NOUSER            // All USER defines and routines
//#define NONLS             // All NLS defines and routines
//#define NOMB              // MB_* and MessageBox()
#define NOMEMMGR          // GMEM_*, LMEM_*, GHND, LHND, associated routines
#define NOMETAFILE        // typedef METAFILEPICT
#define NOMINMAX          // Macros min(a,b) and max(a,b)
//#define NOMSG             // typedef MSG and associated routines
#define NOOPENFILE        // OpenFile(), OemToAnsi, AnsiToOem, and OF_*
#define NOSCROLL          // SB_* and scrolling routines
#define NOSERVICE         // All Service Controller routines, SERVICE_ equates, etc.
#define NOSOUND           // Sound driver routines
#define NOTEXTMETRIC      // typedef TEXTMETRIC and associated routines
//#define NOWH              // SetWindowsHook and WH_*
//#define NOWINOFFSETS      // GWL_*, GCL_*, associated routines
#define NOCOMM            // COMM driver routines
#define NOKANJI           // Kanji support stuff.
#define NOHELP            // Help engine interface.
#define NOPROFILER        // Profiler interface.
#define NODEFERWINDOWPOS  // DeferWindowPos routines
#define NOMCX             // Modem Configuration Extensions

#include <windows.h>
#include <shellapi.h>

#define VK_USE_PLATFORM_WIN32_KHR
#define VK_NO_PROTOTYPES
#include "vulkan.h"

#ifdef VULWF_IMPLEMENTATION
#define VKF(f) PFN_##f f;
#else
#define VKF(f) extern PFN_##f f;
#endif

VKF( vkGetInstanceProcAddr )

VKF( vkCreateInstance )
VKF( vkEnumerateInstanceExtensionProperties )
#ifdef VULWF_USE_DEBUG_REPORTING
VKF( vkEnumerateInstanceLayerProperties )
#endif

VKF( vkEnumeratePhysicalDevices )
VKF( vkGetPhysicalDeviceProperties )
VKF( vkGetPhysicalDeviceFeatures )
VKF( vkGetPhysicalDeviceQueueFamilyProperties )
VKF( vkGetPhysicalDeviceFormatProperties )
VKF( vkCreateDevice )
VKF( vkGetDeviceProcAddr )
VKF( vkDestroyInstance )
VKF( vkEnumerateDeviceExtensionProperties )
VKF( vkGetPhysicalDeviceMemoryProperties )

#ifdef VULWF_USE_DEBUG_REPORTING
VKF( vkCreateDebugReportCallbackEXT )
VKF( vkDestroyDebugReportCallbackEXT )
VKF( vkDebugReportMessageEXT )
#endif

VKF( vkCreateWin32SurfaceKHR )
VKF( vkGetPhysicalDeviceSurfaceSupportKHR )
VKF( vkGetPhysicalDeviceSurfaceCapabilitiesKHR )
VKF( vkGetPhysicalDeviceSurfaceFormatsKHR )
VKF( vkGetPhysicalDeviceSurfacePresentModesKHR )
VKF( vkDestroySurfaceKHR )

VKF( vkGetDeviceQueue )
VKF( vkDeviceWaitIdle )
VKF( vkDestroyDevice )
VKF( vkCreateSemaphore )
VKF( vkCreateCommandPool )
VKF( vkAllocateCommandBuffers )
VKF( vkResetCommandBuffer )
VKF( vkBeginCommandBuffer )
VKF( vkCmdPipelineBarrier )
VKF( vkCmdClearColorImage )
VKF( vkEndCommandBuffer )
VKF( vkQueueSubmit )
VKF( vkFreeCommandBuffers )
VKF( vkDestroyCommandPool )
VKF( vkDestroySemaphore )
VKF( vkCreateSwapchainKHR )
VKF( vkGetSwapchainImagesKHR )
VKF( vkAcquireNextImageKHR )
VKF( vkQueuePresentKHR )
VKF( vkDestroySwapchainKHR )
VKF( vkGetImageMemoryRequirements )
VKF( vkBindImageMemory )
VKF( vkCreateImage )
VKF( vkDestroyImage )
VKF( vkCreateImageView )
VKF( vkCreateSampler )
VKF( vkCreateRenderPass )
VKF( vkCreateFramebuffer )
VKF( vkCreateShaderModule )
VKF( vkCreateDescriptorSetLayout )
VKF( vkCreatePipelineLayout )
VKF( vkCreateDescriptorPool )
VKF( vkAllocateDescriptorSets )
VKF( vkUpdateDescriptorSets )
VKF( vkCreateGraphicsPipelines )
VKF( vkCmdBeginRenderPass )
VKF( vkCmdNextSubpass )
VKF( vkCmdBindPipeline )
VKF( vkCmdBindDescriptorSets )
VKF( vkCmdDraw )
VKF( vkCmdDrawIndexed )
VKF( vkCmdPushConstants )
VKF( vkCmdEndRenderPass )
VKF( vkCmdCopyBuffer )
VKF( vkCmdCopyBufferToImage )
VKF( vkDestroyShaderModule )
VKF( vkDestroyPipelineLayout )
VKF( vkDestroyDescriptorPool )
VKF( vkDestroyDescriptorSetLayout )
VKF( vkDestroyPipeline )
VKF( vkDestroyRenderPass )
VKF( vkDestroyFramebuffer )
VKF( vkDestroyImageView )
VKF( vkDestroySampler )
VKF( vkCreateFence )
VKF( vkCreateBuffer )
VKF( vkGetBufferMemoryRequirements )
VKF( vkAllocateMemory )
VKF( vkBindBufferMemory )
VKF( vkMapMemory )
VKF( vkUnmapMemory )
VKF( vkCmdSetViewport )
VKF( vkCmdSetScissor )
VKF( vkCmdBindVertexBuffers )
VKF( vkCmdBindIndexBuffer )
VKF( vkWaitForFences )
VKF( vkQueueWaitIdle )
VKF( vkResetFences )
VKF( vkFreeMemory )
VKF( vkDestroyBuffer )
VKF( vkDestroyFence )
VKF( vkCreatePipelineCache )
VKF( vkGetPipelineCacheData )
VKF( vkDestroyPipelineCache )

#undef VKF

namespace vulwf
{
	// unique vulkan handle - RAII wrapper for vulkan resource handles
	template<typename T>
	struct uh
	{
		explicit uh( T h = VK_NULL_HANDLE ) noexcept
			: h_( h )
		{
		}
		T * init_ptr() noexcept
		{
			assert( h_ == VK_NULL_HANDLE );
			return &h_;
		}

		T const * addr() noexcept
		{
			assert( h_ != VK_NULL_HANDLE );
			return &h_;
		}

		uh( uh const & ) = delete;
		uh & operator=( uh const & ) = delete;

		uh( uh && other ) noexcept
			: h_( other.h_ )
		{
			other.h_ = VK_NULL_HANDLE; 
		}
		uh & operator=( uh && other ) noexcept
		{
			assert( this != &other );
			this->~uh();
			new(this) uh( std::move( other ) );
			return *this;
		}

		~uh() noexcept;

		void reset( T h = VK_NULL_HANDLE ) noexcept
		{
			assert( h == VK_NULL_HANDLE || h != h_ );
			this->~uh();
			new(this) uh( h );
		}

		void swap( uh & other ) noexcept
		{ std::swap( h_, other.h_ ); }

		operator T() const noexcept
		{ return h_; }

	private:
		T h_;
	};

	template<typename T>
	void swap( uh<T> & a, uh<T> & b ) noexcept
	{ a.swap( b ); }

	struct vulkan_module
	{
		vulkan_module( vulkan_module const & ) = delete;
		vulkan_module & operator=( vulkan_module const & ) = delete;

		vulkan_module() 
			: h_( LoadLibraryW( L"vulkan-1.dll" ) )
		{ if( !h_ ) VULWF_THROW( "Failed to load vulkan-1.dll." ); }
		~vulkan_module() 
		{ FreeLibrary( h_ ); }

		HMODULE const h_;
	};

	struct vk_instance
	{
		vk_instance( vk_instance const & ) = delete;
		vk_instance & operator=( vk_instance const & ) = delete;

		vk_instance( vulkan_module & module, char const * app_name )
		{
			vkGetInstanceProcAddr = (PFN_vkGetInstanceProcAddr)GetProcAddress( module.h_, "vkGetInstanceProcAddr" );
			if( !vkGetInstanceProcAddr )
				VULWF_THROW( "Failed to load vkGetInstanceProcAddr" );

#define GPA(proc) proc = (PFN_##proc)vkGetInstanceProcAddr( nullptr, #proc ); if( !proc ) VULWF_THROW( "Failed to load " #proc );
			GPA( vkCreateInstance )
			GPA( vkEnumerateInstanceExtensionProperties )
#ifdef VULWF_USE_DEBUG_REPORTING
			GPA( vkEnumerateInstanceLayerProperties )
#endif
#undef GPA

			VkApplicationInfo app_info = { VK_STRUCTURE_TYPE_APPLICATION_INFO };
			app_info.pApplicationName = app_name;
			app_info.engineVersion = 1;
			app_info.apiVersion = VK_API_VERSION_1_0;

			VkInstanceCreateInfo instance_info = { VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO };
			instance_info.pApplicationInfo = &app_info;

#ifdef VULWF_USE_DEBUG_REPORTING
			uint32_t layers_count = 0;
			vkEnumerateInstanceLayerProperties( &layers_count, nullptr );
			auto layers = (VkLayerProperties *)alloca( sizeof(VkLayerProperties)*layers_count );
			vkEnumerateInstanceLayerProperties( &layers_count, layers );

			for( size_t i = 0; i != layers_count; ++i )
			{
				if( strcmp( layers[i].layerName, "VK_LAYER_LUNARG_standard_validation" ) == 0 )
					goto found_std_validation;
			}
			VULWF_THROW( "VK_LAYER_LUNARG_standard_validation not found" );
		found_std_validation:

			char const * const enabled_layers[] = { "VK_LAYER_LUNARG_standard_validation" };
			instance_info.enabledLayerCount = sizeof(enabled_layers)/sizeof(enabled_layers[0]);
			instance_info.ppEnabledLayerNames = enabled_layers;
#endif

			uint32_t extensions_count = 0;
			vkEnumerateInstanceExtensionProperties( nullptr, &extensions_count, nullptr );
			auto extensions = (VkExtensionProperties *)alloca( sizeof(VkExtensionProperties)*extensions_count );
			vkEnumerateInstanceExtensionProperties( nullptr, &extensions_count, extensions );

			char const * const enabled_extensions[] = { "VK_KHR_surface", "VK_KHR_win32_surface"
#ifdef VULWF_USE_DEBUG_REPORTING
				, "VK_EXT_debug_report"
#endif
				};

			for( auto const & ext : enabled_extensions )
			{
				for( size_t i = 0; i != extensions_count; ++i )
					if( strcmp( extensions[i].extensionName, ext ) == 0 )
						goto found_requred_ext;
				VULWF_THROW( std::string( ext ) + " not found" );
			found_requred_ext:
				;
			}

			instance_info.enabledExtensionCount = sizeof(enabled_extensions)/sizeof(enabled_extensions[0]);
			instance_info.ppEnabledExtensionNames = enabled_extensions;
			if( vkCreateInstance( &instance_info, nullptr, &instance_ ) != VK_SUCCESS )
				VULWF_THROW( "vkCreateInstance failed" );

#define GPA(proc) proc = (PFN_##proc)vkGetInstanceProcAddr( instance_, #proc ); if( !proc ) { vkDestroyInstance( instance_, nullptr ); VULWF_THROW( "Failed to load " #proc ); }
			GPA( vkEnumeratePhysicalDevices )
			GPA( vkGetPhysicalDeviceProperties )
			GPA( vkGetPhysicalDeviceFeatures )
			GPA( vkGetPhysicalDeviceQueueFamilyProperties )
			GPA( vkGetPhysicalDeviceFormatProperties )
			GPA( vkCreateDevice )
			GPA( vkGetDeviceProcAddr )
			GPA( vkDestroyInstance )
			GPA( vkEnumerateDeviceExtensionProperties )
			GPA( vkGetPhysicalDeviceMemoryProperties )

#ifdef VULWF_USE_DEBUG_REPORTING
			GPA( vkCreateDebugReportCallbackEXT )
			GPA( vkDestroyDebugReportCallbackEXT )
			GPA( vkDebugReportMessageEXT )
#endif

			GPA( vkCreateWin32SurfaceKHR );
			GPA( vkGetPhysicalDeviceSurfaceSupportKHR );
			GPA( vkGetPhysicalDeviceSurfaceFormatsKHR );
			GPA( vkGetPhysicalDeviceSurfaceCapabilitiesKHR );
			GPA( vkGetPhysicalDeviceSurfacePresentModesKHR );
			GPA( vkDestroySurfaceKHR )

			GPA( vkGetDeviceQueue )
			GPA( vkDeviceWaitIdle )
			GPA( vkDestroyDevice )
			GPA( vkCreateSemaphore )
			GPA( vkCreateCommandPool )
			GPA( vkAllocateCommandBuffers )
			GPA( vkResetCommandBuffer )
			GPA( vkBeginCommandBuffer )
			GPA( vkCmdPipelineBarrier )
			GPA( vkCmdClearColorImage )
			GPA( vkEndCommandBuffer )
			GPA( vkQueueSubmit )
			GPA( vkFreeCommandBuffers )
			GPA( vkDestroyCommandPool )
			GPA( vkDestroySemaphore )
			GPA( vkCreateSwapchainKHR )
			GPA( vkGetSwapchainImagesKHR )
			GPA( vkAcquireNextImageKHR )
			GPA( vkQueuePresentKHR )
			GPA( vkDestroySwapchainKHR )
			GPA( vkGetImageMemoryRequirements )
			GPA( vkBindImageMemory )
			GPA( vkCreateImage )
			GPA( vkDestroyImage )
			GPA( vkCreateImageView )
			GPA( vkCreateSampler )
			GPA( vkCreateRenderPass )
			GPA( vkCreateFramebuffer )
			GPA( vkCreateShaderModule )
			GPA( vkCreateDescriptorSetLayout )
			GPA( vkCreatePipelineLayout )
			GPA( vkCreateDescriptorPool )
			GPA( vkAllocateDescriptorSets )
			GPA( vkUpdateDescriptorSets )
			GPA( vkCreateGraphicsPipelines )
			GPA( vkCmdBeginRenderPass )
			GPA( vkCmdNextSubpass )
			GPA( vkCmdBindPipeline )
			GPA( vkCmdBindDescriptorSets )
			GPA( vkCmdDraw )
			GPA( vkCmdDrawIndexed )
			GPA( vkCmdPushConstants )
			GPA( vkCmdEndRenderPass )
			GPA( vkCmdCopyBuffer )
			GPA( vkCmdCopyBufferToImage )
			GPA( vkDestroyShaderModule )
			GPA( vkDestroyPipelineLayout )
			GPA( vkDestroyDescriptorPool )
			GPA( vkDestroyDescriptorSetLayout )
			GPA( vkDestroyPipeline )
			GPA( vkDestroyRenderPass )
			GPA( vkDestroyFramebuffer )
			GPA( vkDestroyImageView )
			GPA( vkDestroySampler )
			GPA( vkCreateFence )
			GPA( vkCreateBuffer )
			GPA( vkGetBufferMemoryRequirements )
			GPA( vkAllocateMemory )
			GPA( vkBindBufferMemory )
			GPA( vkMapMemory )
			GPA( vkUnmapMemory )
			GPA( vkCmdSetViewport )
			GPA( vkCmdSetScissor )
			GPA( vkCmdBindVertexBuffers )
			GPA( vkCmdBindIndexBuffer )
			GPA( vkWaitForFences )
			GPA( vkQueueWaitIdle )
			GPA( vkResetFences )
			GPA( vkFreeMemory )
			GPA( vkDestroyBuffer )
			GPA( vkDestroyFence )
			GPA( vkCreatePipelineCache )
			GPA( vkGetPipelineCacheData )
			GPA( vkDestroyPipelineCache )
#undef GPA
		}
		~vk_instance()
		{
			vkDestroyInstance( instance_, nullptr );
		}

		static VkInstance instance_;
	};

#ifdef VULWF_IMPLEMENTATION
	VkInstance vk_instance::instance_;
#endif

#ifdef VULWF_USE_DEBUG_REPORTING
	template<> inline uh<VkDebugReportCallbackEXT>::~uh() noexcept
	{ if( h_ ) vkDestroyDebugReportCallbackEXT( vk_instance::instance_, h_, nullptr ); }
	inline uh<VkDebugReportCallbackEXT> create_vk_debuglog( PFN_vkDebugReportCallbackEXT callback, void * user_data = nullptr )
	{
		VkDebugReportCallbackCreateInfoEXT callbackinfo = { VK_STRUCTURE_TYPE_DEBUG_REPORT_CREATE_INFO_EXT };
		callbackinfo.flags =  VK_DEBUG_REPORT_ERROR_BIT_EXT | VK_DEBUG_REPORT_WARNING_BIT_EXT | VK_DEBUG_REPORT_PERFORMANCE_WARNING_BIT_EXT;
		callbackinfo.pfnCallback = callback;
		callbackinfo.pUserData = user_data;
		uh<VkDebugReportCallbackEXT> debuglog;
		if( vkCreateDebugReportCallbackEXT( vk_instance::instance_, &callbackinfo, nullptr, debuglog.init_ptr() ) != VK_SUCCESS )
			VULWF_THROW( "vkCreateDebugReportCallbackEXT failed" );
		return debuglog;
	}
#endif

	template<> inline uh<VkSurfaceKHR>::~uh() noexcept
	{ if( h_ ) vkDestroySurfaceKHR( vk_instance::instance_, h_, nullptr ); }
	inline uh<VkSurfaceKHR> create_vk_surface( HWND hwnd )
	{
		VkWin32SurfaceCreateInfoKHR surfaceinfo = { VK_STRUCTURE_TYPE_WIN32_SURFACE_CREATE_INFO_KHR };
		surfaceinfo.hinstance = GetModuleHandleW( nullptr );
		surfaceinfo.hwnd = hwnd;
		uh<VkSurfaceKHR> surface;
		if( vkCreateWin32SurfaceKHR( vk_instance::instance_, &surfaceinfo, nullptr, surface.init_ptr() ) != VK_SUCCESS )
			VULWF_THROW( "vkCreateWin32SurfaceKHR failed" );
		return surface;
	}

	struct single_vk_device
	{
		single_vk_device( VkSurfaceKHR surface, VkPhysicalDeviceFeatures const & features = {} )
		{
			uint32_t phys_devices_count = 0;
			vkEnumeratePhysicalDevices( vk_instance::instance_, &phys_devices_count, nullptr );
			VkPhysicalDevice * phys_devices = (VkPhysicalDevice *)alloca( sizeof(VkPhysicalDevice)*phys_devices_count );
			vkEnumeratePhysicalDevices( vk_instance::instance_, &phys_devices_count, phys_devices );

			for( uint32_t i = 0; i != phys_devices_count; ++i )
			{
				VkPhysicalDeviceProperties device_props = {};
				vkGetPhysicalDeviceProperties( phys_devices[i], &device_props );

				uint32_t queue_family_count = 0;
				vkGetPhysicalDeviceQueueFamilyProperties( phys_devices[i], &queue_family_count, nullptr );
				VkQueueFamilyProperties * queue_family_props = (VkQueueFamilyProperties *)alloca( sizeof(VkQueueFamilyProperties)*queue_family_count );
				vkGetPhysicalDeviceQueueFamilyProperties( phys_devices[i], &queue_family_count, queue_family_props );

				for( uint32_t j = 0; j != queue_family_count; ++j )
				{
					VkBool32 supports_surface;
					vkGetPhysicalDeviceSurfaceSupportKHR( phys_devices[i], j, surface, &supports_surface );

					if( supports_surface && queue_family_props[j].queueFlags & VK_QUEUE_GRAPHICS_BIT )
					{
						phys_device_ = phys_devices[i];
						phys_device_props_ = device_props;
						queue_family_index_ = j;
						goto found_device_queue;
					}
				}
			}
			VULWF_THROW( "Failed to find Vulkan physical device" );
		found_device_queue:

			vkGetPhysicalDeviceMemoryProperties( phys_device_, &memory_properties_ );

			VkDeviceQueueCreateInfo queue_info = { VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO };
			queue_info.queueFamilyIndex = queue_family_index_;
			queue_info.queueCount = 1;
			float const queue_priorities[] = { 1.0f };
			queue_info.pQueuePriorities = queue_priorities;

			VkDeviceCreateInfo device_info = { VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO };
			device_info.queueCreateInfoCount = 1;
			device_info.pQueueCreateInfos = &queue_info;

#ifdef VULWF_USE_DEBUG_REPORTING
			char const * const enabled_layers[] = { "VK_LAYER_LUNARG_standard_validation" };
			device_info.enabledLayerCount = sizeof(enabled_layers)/sizeof(enabled_layers[0]);
			device_info.ppEnabledLayerNames = enabled_layers;
#endif

			char const * const device_extensions[] = { "VK_KHR_swapchain" };
			device_info.enabledExtensionCount = sizeof(device_extensions)/sizeof(device_extensions[0]);
			device_info.ppEnabledExtensionNames = device_extensions;

			device_info.pEnabledFeatures = &features;

			if( vkCreateDevice( phys_device_, &device_info, nullptr, &device_ ) != VK_SUCCESS )
				VULWF_THROW( "vkCreateDevice failed" );

#define GPA(proc) proc = (PFN_##proc)vkGetDeviceProcAddr( device_, #proc ); if( !proc ) { vkDestroyDevice( device_, nullptr ); VULWF_THROW( "Failed to load " #proc ); }
			GPA( vkGetDeviceQueue )
			GPA( vkDeviceWaitIdle )
			GPA( vkDestroyDevice )
			GPA( vkCreateSemaphore )
			GPA( vkCreateCommandPool )
			GPA( vkAllocateCommandBuffers )
			GPA( vkResetCommandBuffer )
			GPA( vkBeginCommandBuffer )
			GPA( vkCmdPipelineBarrier )
			GPA( vkCmdClearColorImage )
			GPA( vkEndCommandBuffer )
			GPA( vkQueueSubmit )
			GPA( vkFreeCommandBuffers )
			GPA( vkDestroyCommandPool )
			GPA( vkDestroySemaphore )
			GPA( vkCreateSwapchainKHR )
			GPA( vkGetSwapchainImagesKHR )
			GPA( vkAcquireNextImageKHR )
			GPA( vkQueuePresentKHR )
			GPA( vkDestroySwapchainKHR )
			GPA( vkGetImageMemoryRequirements )
			GPA( vkBindImageMemory )
			GPA( vkCreateImage )
			GPA( vkDestroyImage )
			GPA( vkCreateImageView )
			GPA( vkCreateSampler )
			GPA( vkCreateRenderPass )
			GPA( vkCreateFramebuffer )
			GPA( vkCreateShaderModule )
			GPA( vkCreateDescriptorSetLayout )
			GPA( vkCreatePipelineLayout )
			GPA( vkCreateDescriptorPool )
			GPA( vkAllocateDescriptorSets )
			GPA( vkUpdateDescriptorSets )
			GPA( vkCreateGraphicsPipelines )
			GPA( vkCmdBeginRenderPass )
			GPA( vkCmdNextSubpass )
			GPA( vkCmdBindPipeline )
			GPA( vkCmdBindDescriptorSets )
			GPA( vkCmdDraw )
			GPA( vkCmdDrawIndexed )
			GPA( vkCmdPushConstants )
			GPA( vkCmdEndRenderPass )
			GPA( vkCmdCopyBuffer )
			GPA( vkCmdCopyBufferToImage )
			GPA( vkDestroyShaderModule )
			GPA( vkDestroyPipelineLayout )
			GPA( vkDestroyDescriptorPool )
			GPA( vkDestroyDescriptorSetLayout )
			GPA( vkDestroyPipeline )
			GPA( vkDestroyRenderPass )
			GPA( vkDestroyFramebuffer )
			GPA( vkDestroyImageView )
			GPA( vkDestroySampler )
			GPA( vkCreateFence )
			GPA( vkCreateBuffer )
			GPA( vkGetBufferMemoryRequirements )
			GPA( vkAllocateMemory )
			GPA( vkBindBufferMemory )
			GPA( vkMapMemory )
			GPA( vkUnmapMemory )
			GPA( vkCmdSetViewport )
			GPA( vkCmdSetScissor )
			GPA( vkCmdBindVertexBuffers )
			GPA( vkCmdBindIndexBuffer )
			GPA( vkWaitForFences )
			GPA( vkQueueWaitIdle )
			GPA( vkResetFences )
			GPA( vkFreeMemory )
			GPA( vkDestroyBuffer )
			GPA( vkDestroyFence )
			GPA( vkCreatePipelineCache )
			GPA( vkGetPipelineCacheData )
			GPA( vkDestroyPipelineCache )
#undef GPA

			vkGetDeviceQueue( device_, queue_family_index_, 0, &queue_ );
		}
		~single_vk_device()
		{
			vkDeviceWaitIdle( device_ );
			vkDestroyDevice( device_, nullptr );
		}

		static VkPhysicalDevice phys_device_;
		static VkPhysicalDeviceProperties phys_device_props_;
		static VkDevice device_;
		static VkPhysicalDeviceMemoryProperties memory_properties_;
		static uint32_t queue_family_index_;
		static VkQueue queue_;
	};

#ifdef VULWF_IMPLEMENTATION
	VkPhysicalDevice single_vk_device::phys_device_;
	VkPhysicalDeviceProperties single_vk_device::phys_device_props_;
	VkDevice single_vk_device::device_;
	VkPhysicalDeviceMemoryProperties single_vk_device::memory_properties_;
	uint32_t single_vk_device::queue_family_index_;
	VkQueue single_vk_device::queue_;
#endif

	template<> inline uh<VkCommandPool>::~uh() noexcept
	{ if( h_ ) vkDestroyCommandPool( single_vk_device::device_, h_, nullptr ); }
	inline uh<VkCommandPool> create_vk_cmdpool( VkCommandPoolCreateFlags flags )
	{
		VkCommandPoolCreateInfo cmdpool_info = { VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO };
		cmdpool_info.flags = flags;
		cmdpool_info.queueFamilyIndex = single_vk_device::queue_family_index_;
		uh<VkCommandPool> cmdpool;
		if( vkCreateCommandPool( single_vk_device::device_, &cmdpool_info, nullptr, cmdpool.init_ptr() ) != VK_SUCCESS )
			VULWF_THROW( "vkCreateCommandPool failed" );
		return cmdpool;
	}

	template<> inline uh<VkSwapchainKHR>::~uh() noexcept
	{ if( h_ ) vkDestroySwapchainKHR( single_vk_device::device_, h_, nullptr ); }

	template<> inline uh<VkImage>::~uh() noexcept
	{ if( h_ ) vkDestroyImage( single_vk_device::device_, h_, nullptr ); }

	template<> inline uh<VkImageView>::~uh() noexcept
	{ if( h_ ) vkDestroyImageView( single_vk_device::device_, h_, nullptr ); }

	template<> inline uh<VkFence>::~uh() noexcept
	{ if( h_ ) vkDestroyFence( single_vk_device::device_, h_, nullptr ); }
	inline uh<VkFence> create_vk_fence( bool signaled = false )
	{
		VkFenceCreateInfo fence_info = { VK_STRUCTURE_TYPE_FENCE_CREATE_INFO };
		fence_info.flags = signaled ? VK_FENCE_CREATE_SIGNALED_BIT : 0;
		uh<VkFence> f;
		if( vkCreateFence( single_vk_device::device_, &fence_info, nullptr, f.init_ptr() ) != VK_SUCCESS )
			VULWF_THROW( "vkCreateFence failed" );
		return f;
	}

	inline VkCommandBuffer create_vk_cmdbuf( VkCommandPool cmdpool, VkCommandBufferLevel level = VK_COMMAND_BUFFER_LEVEL_PRIMARY )
	{
		VkCommandBufferAllocateInfo const cmdbuf_info = { VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO, nullptr, cmdpool, level, 1 };
		VkCommandBuffer cmdbuf;
		if( vkAllocateCommandBuffers( single_vk_device::device_, &cmdbuf_info, &cmdbuf ) != VK_SUCCESS )
			VULWF_THROW( "vkAllocateCommandBuffers failed" );
		return cmdbuf;
	}

	template<> inline uh<VkDeviceMemory>::~uh() noexcept
	{ if( h_ ) vkFreeMemory( single_vk_device::device_, h_, nullptr ); }

	inline uint32_t get_mem_type_index( uint32_t mem_type_bits, VkMemoryPropertyFlags desired_mem_flags )
	{
		// mem_type_bits is a bitfield where if bit i is set, it means that 
		// the VkMemoryType i of the VkPhysicalDeviceMemoryProperties structure 
		// satisfies the memory requirements:
		for( uint32_t i = 0; i != 32; ++i, mem_type_bits = mem_type_bits >> 1 )
		{
			if( ( mem_type_bits & 1 ) 
				&& ( single_vk_device::memory_properties_.memoryTypes[i].propertyFlags & desired_mem_flags ) == desired_mem_flags )
				return i;
		}
		VULWF_THROW( "Can't find suitable memory type index" );
	}
	
	struct vulkan_swapchain
	{
		vulkan_swapchain( VkSurfaceKHR surface, VkFormat desired_color_format = VK_FORMAT_B8G8R8A8_SRGB )
		{
			uint32_t formats_count = 0;
			vkGetPhysicalDeviceSurfaceFormatsKHR( single_vk_device::phys_device_, surface, &formats_count, nullptr );
			VkSurfaceFormatKHR * surface_formats = (VkSurfaceFormatKHR *)alloca( sizeof(VkSurfaceFormatKHR)*formats_count );
			vkGetPhysicalDeviceSurfaceFormatsKHR( single_vk_device::phys_device_, surface, &formats_count, surface_formats );
			selected_format_ = surface_formats[0];
			if( formats_count == 1 && surface_formats[0].format == VK_FORMAT_UNDEFINED )
			{
				selected_format_ = { desired_color_format, VK_COLOR_SPACE_SRGB_NONLINEAR_KHR };
			}
			else
			{
				for( uint32_t i = 0; i != formats_count; ++i )
				{
					if( surface_formats[i].format == desired_color_format )
					{
						selected_format_ = surface_formats[i];
						break;
					}
				}
			}
		}

		void recreate( VkSurfaceKHR surface, std::pair<unsigned,unsigned> const & view_size, bool mailbox = false )
		{
			VkSurfaceCapabilitiesKHR surface_capabilities;
			if( vkGetPhysicalDeviceSurfaceCapabilitiesKHR( single_vk_device::phys_device_, surface, &surface_capabilities ) != VK_SUCCESS )
				VULWF_THROW( "vkGetPhysicalDeviceSurfaceCapabilitiesKHR failed" );

			uint32_t swap_images_count = std::max( 2u, surface_capabilities.minImageCount );
			if( surface_capabilities.maxImageCount != 0 ) 
				swap_images_count = std::min( swap_images_count, surface_capabilities.maxImageCount );

			VkExtent2D swap_res = surface_capabilities.currentExtent;
			if( swap_res.width == -1 || swap_res.height == -1 )
			{
				swap_res.width = view_size.first;
				swap_res.height = view_size.second;
			} 
			else 
			{
				if( view_size.first != swap_res.width || view_size.second != swap_res.height )
					VULWF_THROW( "Vulkan surface resolution doesn't match window size" );
			}

			VkSurfaceTransformFlagBitsKHR const pre_transform = surface_capabilities.supportedTransforms & VK_SURFACE_TRANSFORM_IDENTITY_BIT_KHR ? 
				VK_SURFACE_TRANSFORM_IDENTITY_BIT_KHR : surface_capabilities.currentTransform;

			VkPresentModeKHR present_mode = VK_PRESENT_MODE_FIFO_KHR;

			if( mailbox )
			{
				uint32_t present_modes_count = 0;
				vkGetPhysicalDeviceSurfacePresentModesKHR( single_vk_device::phys_device_, surface, &present_modes_count, nullptr );
				VkPresentModeKHR * present_modes = (VkPresentModeKHR *)alloca( sizeof( VkPresentModeKHR )*present_modes_count );
				vkGetPhysicalDeviceSurfacePresentModesKHR( single_vk_device::phys_device_, surface, &present_modes_count, present_modes );
				for( uint32_t i = 0; i != present_modes_count; ++i )
					if( present_modes[i] == VK_PRESENT_MODE_MAILBOX_KHR )
						present_mode = VK_PRESENT_MODE_MAILBOX_KHR;
			}

			{
				uh<VkSwapchainKHR> old_swapchain = std::move( swapchain_ );

				VkSwapchainCreateInfoKHR swap_chain_info = { VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR };
				swap_chain_info.surface = surface;
				swap_chain_info.minImageCount = swap_images_count;
				swap_chain_info.imageFormat = selected_format_.format;
				swap_chain_info.imageColorSpace = selected_format_.colorSpace;
				swap_chain_info.imageExtent = swap_res;
				swap_chain_info.imageArrayLayers = 1;
				swap_chain_info.imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;
				swap_chain_info.imageSharingMode = VK_SHARING_MODE_EXCLUSIVE;
				swap_chain_info.preTransform = pre_transform;
				swap_chain_info.compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;
				swap_chain_info.presentMode = present_mode;
				swap_chain_info.clipped = VK_TRUE;
				swap_chain_info.oldSwapchain = old_swapchain;
				if( vkCreateSwapchainKHR( single_vk_device::device_, &swap_chain_info, nullptr, swapchain_.init_ptr() ) != VK_SUCCESS )
					VULWF_THROW( "vkCreateSwapchainKHR failed" );

				for( uint32_t i = 0; i != images_count_; ++i )
					image_views_[i].reset();
			}

			images_count_ = 0;
			vkGetSwapchainImagesKHR( single_vk_device::device_, swapchain_, &images_count_, nullptr );
			if( images_count_ > sizeof(images_)/sizeof(images_[0]) )
				VULWF_THROW( "Too many swap chain images needed" );
			vkGetSwapchainImagesKHR( single_vk_device::device_, swapchain_, &images_count_, images_ );

			VkImageViewCreateInfo imgview_info = {};
			imgview_info.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
			imgview_info.viewType = VK_IMAGE_VIEW_TYPE_2D;
			imgview_info.format = selected_format_.format;
			imgview_info.components = { VK_COMPONENT_SWIZZLE_R, VK_COMPONENT_SWIZZLE_G, VK_COMPONENT_SWIZZLE_B, VK_COMPONENT_SWIZZLE_A };
			imgview_info.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
			imgview_info.subresourceRange.baseMipLevel = 0;
			imgview_info.subresourceRange.levelCount = 1;
			imgview_info.subresourceRange.baseArrayLayer = 0;
			imgview_info.subresourceRange.layerCount = 1;

			for( uint32_t i = 0; i != images_count_; ++i )
			{
				imgview_info.image = images_[i];
				if( vkCreateImageView( single_vk_device::device_, &imgview_info, nullptr, image_views_[i].init_ptr() ) != VK_SUCCESS )
					VULWF_THROW( "vkCreateImageView failed" );
			}
		}

		VkSurfaceFormatKHR selected_format_;
		uh<VkSwapchainKHR> swapchain_;
		uint32_t images_count_ = 0;
		VkImage images_[VULWF_MAX_SWAP_IMAGES];
		uh<VkImageView> image_views_[VULWF_MAX_SWAP_IMAGES];
	};
	
	struct vulkan_depthbuf
	{
		void recreate( VkCommandBuffer cmdbuf_setup, std::pair<unsigned,unsigned> const & view_size, VkFormat format = VK_FORMAT_D16_UNORM )
		{
			depth_view_.reset();
			image_memory_.reset();
			depth_image_.reset();

			VkImageCreateInfo image_info = { VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO };
			image_info.imageType = VK_IMAGE_TYPE_2D;
			image_info.format = format;
			image_info.extent = { view_size.first, view_size.second, 1 };
			image_info.mipLevels = 1;
			image_info.arrayLayers = 1;
			image_info.samples = VK_SAMPLE_COUNT_1_BIT;
			image_info.tiling = VK_IMAGE_TILING_OPTIMAL;
			image_info.usage = VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT;
			image_info.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
			image_info.queueFamilyIndexCount = 0;
			image_info.pQueueFamilyIndices = nullptr;
			image_info.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
			if( vkCreateImage( single_vk_device::device_, &image_info, nullptr, depth_image_.init_ptr() ) )
				VULWF_THROW( "vkCreateImage failed" );

			VkMemoryRequirements mem_requirements = {};
			vkGetImageMemoryRequirements( single_vk_device::device_, depth_image_, &mem_requirements );

			VkMemoryAllocateInfo alloc_info = { VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO };
			alloc_info.allocationSize = mem_requirements.size;
			alloc_info.memoryTypeIndex = get_mem_type_index( mem_requirements.memoryTypeBits, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT );

			if( vkAllocateMemory( single_vk_device::device_, &alloc_info, nullptr, image_memory_.init_ptr() ) != VK_SUCCESS )
				VULWF_THROW( "vkAllocateMemory failed" );
			if( vkBindImageMemory( single_vk_device::device_, depth_image_, image_memory_, 0 ) != VK_SUCCESS )
				VULWF_THROW( "vkBindImageMemory failed" );

			{
				VkCommandBufferBeginInfo begin_info = { VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO };
				begin_info.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;

				vkBeginCommandBuffer( cmdbuf_setup, &begin_info );

				VkImageMemoryBarrier undef_to_depth_bar = { VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER };
				undef_to_depth_bar.srcAccessMask = 0;
				undef_to_depth_bar.dstAccessMask = 0;
				undef_to_depth_bar.oldLayout = VK_IMAGE_LAYOUT_UNDEFINED;
				undef_to_depth_bar.newLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;
				undef_to_depth_bar.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
				undef_to_depth_bar.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
				undef_to_depth_bar.image = depth_image_;
				undef_to_depth_bar.subresourceRange = { VK_IMAGE_ASPECT_DEPTH_BIT, 0, 1, 0, 1 };

				vkCmdPipelineBarrier( cmdbuf_setup, 
					VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT, 
					VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT, 
					0,
					0, nullptr,
					0, nullptr, 
					1, &undef_to_depth_bar );

				vkEndCommandBuffer( cmdbuf_setup );

				VkPipelineStageFlags wait_stage_mask[] = { VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT };
				VkSubmitInfo submit_info = { VK_STRUCTURE_TYPE_SUBMIT_INFO };
				submit_info.waitSemaphoreCount = 0;
				submit_info.pWaitSemaphores = nullptr;
				submit_info.pWaitDstStageMask = wait_stage_mask;
				submit_info.commandBufferCount = 1;
				submit_info.pCommandBuffers = &cmdbuf_setup;
				submit_info.signalSemaphoreCount = 0;
				submit_info.pSignalSemaphores = nullptr;
				if( vkQueueSubmit( single_vk_device::queue_, 1, &submit_info, VK_NULL_HANDLE ) != VK_SUCCESS )
					VULWF_THROW( "vkQueueSubmit failed" );

				vkQueueWaitIdle( single_vk_device::queue_ );
				vkResetCommandBuffer( cmdbuf_setup, 0 );
			}

			VkImageViewCreateInfo image_view_info = { VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO };
			image_view_info.image = depth_image_;
			image_view_info.viewType = VK_IMAGE_VIEW_TYPE_2D;
			image_view_info.format = format;
			image_view_info.components = { VK_COMPONENT_SWIZZLE_IDENTITY, VK_COMPONENT_SWIZZLE_IDENTITY, VK_COMPONENT_SWIZZLE_IDENTITY, VK_COMPONENT_SWIZZLE_IDENTITY };
			image_view_info.subresourceRange.aspectMask = VK_IMAGE_ASPECT_DEPTH_BIT;
			image_view_info.subresourceRange.baseMipLevel = 0;
			image_view_info.subresourceRange.levelCount = 1;
			image_view_info.subresourceRange.baseArrayLayer = 0;
			image_view_info.subresourceRange.layerCount = 1;
			if( vkCreateImageView( single_vk_device::device_, &image_view_info, nullptr, depth_view_.init_ptr() ) != VK_SUCCESS )
				VULWF_THROW( "vkCreateImageView failed" );
		}
		~vulkan_depthbuf()
		{
			vkDeviceWaitIdle( single_vk_device::device_ );
		}

		uh<VkImage> depth_image_;
		uh<VkDeviceMemory> image_memory_;
		uh<VkImageView> depth_view_;
	};

	template<> inline uh<VkRenderPass>::~uh() noexcept
	{ if( h_ ) vkDestroyRenderPass( single_vk_device::device_, h_, nullptr ); }
	
	template<> inline uh<VkFramebuffer>::~uh() noexcept
	{ if( h_ ) vkDestroyFramebuffer( single_vk_device::device_, h_, nullptr ); }

	inline std::array<uh<VkFramebuffer>, VULWF_MAX_SWAP_IMAGES> create_swap_frambuffers( VkImageView depth_view, VkRenderPass render_pass, 
		uh<VkImageView> const * swapchain_views, uint32_t swapchain_views_count,
		std::pair<unsigned,unsigned> view_size )
	{
		if( swapchain_views_count > VULWF_MAX_SWAP_IMAGES )
			VULWF_THROW( "Too many swapchain images" );

		VkImageView attachments[2];
		attachments[1] = depth_view;

		VkFramebufferCreateInfo framebuffer_info = { VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO };
		framebuffer_info.renderPass = render_pass;
		framebuffer_info.attachmentCount = 2;
		framebuffer_info.pAttachments = attachments;
		framebuffer_info.width = view_size.first;
		framebuffer_info.height = view_size.second;
		framebuffer_info.layers = 1;

		std::array<uh<VkFramebuffer>, VULWF_MAX_SWAP_IMAGES> fbufs;
		for( uint32_t i = 0; i != swapchain_views_count; ++i )
		{
			attachments[0] = swapchain_views[i];
			if( vkCreateFramebuffer( single_vk_device::device_, &framebuffer_info, nullptr, fbufs[i].init_ptr() ) != VK_SUCCESS )
				VULWF_THROW( "vkCreateFramebuffer failed" );			
		}

		return fbufs;
	}

	template<> inline uh<VkBuffer>::~uh() noexcept
	{ if( h_ ) vkDestroyBuffer( single_vk_device::device_, h_, nullptr ); }

	inline std::pair<uh<VkBuffer>, uh<VkDeviceMemory>> create_buffer( void const * data, size_t data_size, VkBufferUsageFlags usage, 
		VkMemoryPropertyFlags desired_mem_flags )
	{
		VkBufferCreateInfo buffer_info = { VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO };
		buffer_info.size = data_size;
		buffer_info.usage = usage;
		buffer_info.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
		buffer_info.queueFamilyIndexCount = 0;
		buffer_info.pQueueFamilyIndices = nullptr;

		uh<VkBuffer> buf;
		if( vkCreateBuffer( single_vk_device::device_, &buffer_info, nullptr, buf.init_ptr() ) != VK_SUCCESS )
			VULWF_THROW( "vkCreateBuffer failed" );

		VkMemoryRequirements mem_requirements = {};
		vkGetBufferMemoryRequirements( single_vk_device::device_, buf, &mem_requirements );

		VkMemoryAllocateInfo alloc_info = { VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO };
		alloc_info.allocationSize = mem_requirements.size;
		alloc_info.memoryTypeIndex = get_mem_type_index( mem_requirements.memoryTypeBits, desired_mem_flags );

		uh<VkDeviceMemory> buffer_memory;
		if( vkAllocateMemory( single_vk_device::device_, &alloc_info, nullptr, buffer_memory.init_ptr() ) != VK_SUCCESS )
			VULWF_THROW( "vkAllocateMemory failed" );

		if( data )
		{
			void * mapped;
			if( vkMapMemory( single_vk_device::device_, buffer_memory, 0, VK_WHOLE_SIZE, 0, &mapped ) != VK_SUCCESS )
				VULWF_THROW( "vkMapMemory failed" );
			memcpy( mapped, data, data_size );
			vkUnmapMemory( single_vk_device::device_, buffer_memory );
		}

		if( vkBindBufferMemory( single_vk_device::device_, buf, buffer_memory, 0 ) != VK_SUCCESS )
			VULWF_THROW( "vkBindBufferMemory failed" );

		return { std::move(buf), std::move(buffer_memory) };
	}

	inline std::pair<uh<VkBuffer>, uh<VkDeviceMemory>> create_static_buffer( void const * data, size_t data_size, VkBufferUsageFlags usage, 
		VkCommandBuffer cmd_copy, VkQueue queue_copy )
	{
		auto copy_src = create_buffer( data, data_size, VK_BUFFER_USAGE_TRANSFER_SRC_BIT, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT );
		auto copy_dst = create_buffer( nullptr, data_size, usage | VK_BUFFER_USAGE_TRANSFER_DST_BIT, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT );

		VkCommandBufferBeginInfo begin_info = { VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO };
		begin_info.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;

		vkBeginCommandBuffer( cmd_copy, &begin_info );
		VkBufferCopy const copy_region = { 0, 0, data_size };
		vkCmdCopyBuffer( cmd_copy,
			copy_src.first,
			copy_dst.first,
			1, &copy_region );
		vkEndCommandBuffer( cmd_copy );

		VkSubmitInfo submit_info = { VK_STRUCTURE_TYPE_SUBMIT_INFO };
		submit_info.commandBufferCount = 1;
		submit_info.pCommandBuffers = &cmd_copy;
		if( vkQueueSubmit( queue_copy, 1, &submit_info, VK_NULL_HANDLE ) != VK_SUCCESS )
			VULWF_THROW( "vkQueueSubmit failed" );

		vkQueueWaitIdle( queue_copy );
		vkResetCommandBuffer( cmd_copy, 0 );

		return copy_dst;
	}

	template<> inline uh<VkSampler>::~uh() noexcept
	{ if( h_ ) vkDestroySampler( single_vk_device::device_, h_, nullptr ); }

	inline std::tuple<uh<VkImage>, uh<VkDeviceMemory>, uh<VkImageView>> create_texture( 
		VkFormat format, std::pair<uint32_t, uint32_t> size,
		std::pair<void const *, uint32_t> const * mip_levels, uint32_t mip_levels_count,
		VkCommandBuffer cmd_copy, VkQueue queue_copy )
	{
		VkBufferImageCopy * copy_regions = (VkBufferImageCopy *)alloca( sizeof(VkBufferImageCopy)*mip_levels_count );

		uint32_t data_size = 0;
		for( uint32_t i = 0; i != mip_levels_count; ++i )
			data_size += mip_levels[i].second;

		auto copy_src = create_buffer( nullptr, data_size, VK_BUFFER_USAGE_TRANSFER_SRC_BIT, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT );

		{
			void * mapped;
			if( vkMapMemory( vulwf::single_vk_device::device_, copy_src.second, 0, VK_WHOLE_SIZE, 0, &mapped ) != VK_SUCCESS )
				VULWF_THROW( "vkMapMemory failed" );
			char * ptr = reinterpret_cast<char *>(mapped);

			auto mip_size = size;
			for( uint32_t i = 0; i != mip_levels_count; ++i )
			{
				memcpy( ptr, mip_levels[i].first, mip_levels[i].second );

				VkBufferImageCopy mip_copy ={};
				mip_copy.bufferOffset = ptr - (char *)mapped;
				mip_copy.imageSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
				mip_copy.imageSubresource.mipLevel = i;
				mip_copy.imageSubresource.baseArrayLayer = 0;
				mip_copy.imageSubresource.layerCount = 1;
				mip_copy.imageExtent.width = mip_size.first;
				mip_copy.imageExtent.height = mip_size.second;
				mip_copy.imageExtent.depth = 1;

				copy_regions[i] = mip_copy;

				ptr += mip_levels[i].second;
				mip_size.first = std::max( mip_size.first/2, 1u );
				mip_size.second = std::max( mip_size.second/2, 1u );
			}
		
			vkUnmapMemory( vulwf::single_vk_device::device_, copy_src.second );
		}

		VkImageCreateInfo image_info = { VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO };
		image_info.imageType = VK_IMAGE_TYPE_2D;
		image_info.format = format;
		image_info.mipLevels = mip_levels_count;
		image_info.arrayLayers = 1;
		image_info.samples = VK_SAMPLE_COUNT_1_BIT;
		image_info.tiling = VK_IMAGE_TILING_OPTIMAL;
		image_info.usage = VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_SAMPLED_BIT;
		image_info.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
		image_info.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
		image_info.extent = { size.first, size.second, 1 };

		uh<VkImage> image;
		if( vkCreateImage( vulwf::single_vk_device::device_, &image_info, nullptr, image.init_ptr() ) != VK_SUCCESS )
			VULWF_THROW( "vkCreateImage failed" );

		VkMemoryRequirements mem_requirements = {};
		vkGetImageMemoryRequirements( vulwf::single_vk_device::device_, image, &mem_requirements );

		VkMemoryAllocateInfo alloc_info = { VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO };
		alloc_info.allocationSize = mem_requirements.size;
		alloc_info.memoryTypeIndex = get_mem_type_index( mem_requirements.memoryTypeBits, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT );

		uh<VkDeviceMemory> image_memory;
		if( vkAllocateMemory( single_vk_device::device_, &alloc_info, nullptr, image_memory.init_ptr() ) != VK_SUCCESS )
			VULWF_THROW( "vkAllocateMemory failed" );
		if( vkBindImageMemory( single_vk_device::device_, image, image_memory, 0 ) != VK_SUCCESS )
			VULWF_THROW( "vkBindImageMemory failed" );

		VkCommandBufferBeginInfo begin_info = { VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO };
		begin_info.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;

		vkBeginCommandBuffer( cmd_copy, &begin_info );

		VkImageMemoryBarrier transition_dst = { VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER };
		transition_dst.srcAccessMask = 0;
		transition_dst.dstAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
		transition_dst.oldLayout = VK_IMAGE_LAYOUT_UNDEFINED;
		transition_dst.newLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
		transition_dst.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
		transition_dst.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
		transition_dst.image = image;
		transition_dst.subresourceRange = { VK_IMAGE_ASPECT_COLOR_BIT, 0, mip_levels_count, 0, 1 };

		vkCmdPipelineBarrier( cmd_copy, 
			VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT, 
			VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT, 
			0,
			0, nullptr,
			0, nullptr, 
			1, &transition_dst );
		
		vkCmdCopyBufferToImage( cmd_copy,
			copy_src.first, image,
			VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
			mip_levels_count, copy_regions );
		
		VkImageMemoryBarrier transition_read = { VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER };
		transition_read.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
		transition_read.dstAccessMask = VK_ACCESS_SHADER_READ_BIT;
		transition_read.oldLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
		transition_read.newLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
		transition_read.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
		transition_read.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
		transition_read.image = image;
		transition_read.subresourceRange = { VK_IMAGE_ASPECT_COLOR_BIT, 0, mip_levels_count, 0, 1 };

		vkCmdPipelineBarrier( cmd_copy, 
			VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT, 
			VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT, 
			0,
			0, nullptr,
			0, nullptr, 
			1, &transition_read );

		vkEndCommandBuffer( cmd_copy );

		VkSubmitInfo submit_info = { VK_STRUCTURE_TYPE_SUBMIT_INFO };
		submit_info.commandBufferCount = 1;
		submit_info.pCommandBuffers = &cmd_copy;
		if( vkQueueSubmit( queue_copy, 1, &submit_info, VK_NULL_HANDLE ) != VK_SUCCESS )
			VULWF_THROW( "vkQueueSubmit failed" );

		vkQueueWaitIdle( queue_copy );
		vkResetCommandBuffer( cmd_copy, 0 );

		VkImageViewCreateInfo image_view_info = { VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO };
		image_view_info.image = image;
		image_view_info.viewType = VK_IMAGE_VIEW_TYPE_2D;
		image_view_info.format = format;
		image_view_info.components = { VK_COMPONENT_SWIZZLE_R, VK_COMPONENT_SWIZZLE_G, VK_COMPONENT_SWIZZLE_B, VK_COMPONENT_SWIZZLE_A };
		image_view_info.subresourceRange = { VK_IMAGE_ASPECT_COLOR_BIT, 0, mip_levels_count, 0, 1 };
		uh<VkImageView> image_view;
		if( vkCreateImageView( single_vk_device::device_, &image_view_info, nullptr, image_view.init_ptr() ) != VK_SUCCESS )
			VULWF_THROW( "vkCreateImageView failed" );

		return { std::move( image ), std::move( image_memory ), std::move( image_view ) };
	}

	inline uh<VkSampler> create_sampler( VkFilter mag_flt, VkFilter min_flt, VkSamplerMipmapMode mipmap_mode, 
		VkSamplerAddressMode addr_mode, 
		float max_aniso = 1.f )
	{
		VkSamplerCreateInfo sampler_info = { VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO };
		sampler_info.magFilter = mag_flt;
		sampler_info.minFilter = min_flt;
		sampler_info.mipmapMode = mipmap_mode;
		sampler_info.addressModeU = addr_mode;
		sampler_info.addressModeV = addr_mode;
		sampler_info.addressModeW = addr_mode;
		sampler_info.mipLodBias = 0.f;
		sampler_info.compareOp = VK_COMPARE_OP_NEVER;
		sampler_info.minLod = 0.f;
		sampler_info.maxLod = 16.f;
		sampler_info.maxAnisotropy = max_aniso;
		sampler_info.anisotropyEnable = max_aniso > 1.f ? VK_TRUE : VK_FALSE;
		sampler_info.borderColor = VK_BORDER_COLOR_FLOAT_TRANSPARENT_BLACK;

		uh<VkSampler> sampler;
		if( vkCreateSampler( single_vk_device::device_, &sampler_info, nullptr, sampler.init_ptr() ) != VK_SUCCESS )
			VULWF_THROW( "vkCreateSampler failed" );

		return sampler;
	}

	template<> inline uh<VkShaderModule>::~uh() noexcept
	{ if( h_ ) vkDestroyShaderModule( single_vk_device::device_, h_, nullptr ); }
	inline uh<VkShaderModule> create_vk_shader_module( uint32_t const * spv, size_t spv_size )
	{
		VkShaderModuleCreateInfo shader_info = { VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO };
		shader_info.codeSize = spv_size;
		shader_info.pCode = spv;

		uh<VkShaderModule> module;
		if( vkCreateShaderModule( single_vk_device::device_, &shader_info, nullptr, module.init_ptr() ) != VK_SUCCESS )
			VULWF_THROW( "vkCreateShaderModule failed" );
		return module;
	}

	template<> inline uh<VkDescriptorSetLayout>::~uh() noexcept
	{ if( h_ ) vkDestroyDescriptorSetLayout( single_vk_device::device_, h_, nullptr ); }

	template<> inline uh<VkDescriptorPool>::~uh() noexcept
	{ if( h_ ) vkDestroyDescriptorPool( single_vk_device::device_, h_, nullptr ); }

	template<typename ...t_pool_sizes>
	uh<VkDescriptorPool> create_descriptor_pool( uint32_t max_sets, bool free_set, t_pool_sizes... descriptors )
	{
		VkDescriptorPoolSize const pool_sizes[] = { descriptors... };

		VkDescriptorPoolCreateInfo pool_info = { VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO };
		pool_info.flags = free_set ? VK_DESCRIPTOR_POOL_CREATE_FREE_DESCRIPTOR_SET_BIT : 0;
		pool_info.maxSets = max_sets;
		pool_info.poolSizeCount = sizeof(pool_sizes)/sizeof(pool_sizes[0]);
		pool_info.pPoolSizes = pool_sizes;

		uh<VkDescriptorPool> descriptor_pool;
		if( vkCreateDescriptorPool( single_vk_device::device_, &pool_info, nullptr, descriptor_pool.init_ptr() ) != VK_SUCCESS )
			VULWF_THROW( "vkCreateDescriptorPool failed" );

		return descriptor_pool;
	}

	template<> inline uh<VkPipelineLayout>::~uh() noexcept
	{ if( h_ ) vkDestroyPipelineLayout( single_vk_device::device_, h_, nullptr ); }
	
	template<> inline uh<VkPipeline>::~uh() noexcept
	{ if( h_ ) vkDestroyPipeline( single_vk_device::device_, h_, nullptr ); }

	template<> inline uh<VkSemaphore>::~uh() noexcept
	{ if( h_ ) vkDestroySemaphore( single_vk_device::device_, h_, nullptr ); }

	template<> inline uh<VkPipelineCache>::~uh() noexcept
	{ if( h_ ) vkDestroyPipelineCache( single_vk_device::device_, h_, nullptr ); }

	struct monitor_info
	{
		int x, y;
		unsigned width, height;
	};
	inline std::vector<monitor_info> get_monitors()
	{
		std::vector<monitor_info> monitors;

		DISPLAY_DEVICEW display = {};
		display.cb = sizeof(DISPLAY_DEVICEW);
		DEVMODEW mode = {};
		mode.dmSize = sizeof(DEVMODEW);

		for( DWORD iadapter = 0; EnumDisplayDevicesW( nullptr, iadapter, &display, 0 ); ++iadapter )
		{
			if( display.StateFlags & DISPLAY_DEVICE_ACTIVE )
			{
				EnumDisplaySettingsW( display.DeviceName, ENUM_CURRENT_SETTINGS, &mode );
				monitors.push_back( { mode.dmPosition.x, mode.dmPosition.y, mode.dmPelsWidth, mode.dmPelsHeight } );
			}
		}

		return monitors;
	}

	// enum for mode parameter in window constructor
	// 'create_fullscreen + i' means fullscreen on monitor i
	enum window_create_mode : int
	{
		create_windowed = -2,
		create_maximized,
		create_fullscreen
	};

	struct window_create_params
	{
		std::pair<int,int> windowed_pos_;
		std::pair<unsigned, unsigned> windowed_viewport_size_;
		int mode_;	// based on window_create_mode
	};

	template<typename t_logic>
	struct window
	{
		window( window const & ) = delete;
		window & operator=( window const & ) = delete;

		template<typename ... t_logic_args>
		window( char const * title_utf8, window_create_params const & create_params_, 
			t_logic_args && ... logic_args )
		{
			should_close_ = false;
			minimized_ = false;
			maximized_ = create_params_.mode_ == create_maximized;
			fullscreen_ = false;

			windowed_pos_ = create_params_.windowed_pos_;

			{
				RECT r = { 0, 0, LONG(create_params_.windowed_viewport_size_.first), LONG(create_params_.windowed_viewport_size_.second) };
				AdjustWindowRectEx( &r, WS_OVERLAPPEDWINDOW, FALSE, 0 );
				windowed_size_.first = r.right - r.left;
				windowed_size_.second = r.bottom - r.top;
			}

			int cw_x = windowed_pos_.first, cw_y = windowed_pos_.second, cw_width = windowed_size_.first, cw_height = windowed_size_.second;
			DWORD cw_ex_style = 0, cw_style = WS_OVERLAPPEDWINDOW;
			
			if( create_params_.mode_ >= create_fullscreen )
			{
				auto const & monitors = get_monitors();
				if( create_params_.mode_ < monitors.size() )
				{
					fullscreen_ = true;
					fs_monitor_ = create_params_.mode_;

					auto const & m = monitors[create_params_.mode_];
					cw_x = m.x;
					cw_y = m.y;
					cw_width = m.width;
					cw_height = m.height;
					cw_ex_style = WS_EX_TOPMOST;
					cw_style = WS_POPUP;
				}
			}

			HINSTANCE const hinstance = GetModuleHandleW( nullptr );

			WNDCLASSW wc;
			wc.style = CS_OWNDC;
			wc.lpfnWndProc = wndproc;
			wc.cbClsExtra = 0;
			wc.cbWndExtra = 0;
			wc.hInstance = hinstance;
			wc.hIcon = LoadIconW( hinstance, L"VULWF_ICON" );
			if( !wc.hIcon )
				wc.hIcon = LoadIconW( nullptr, IDI_WINLOGO );
			wc.hCursor = LoadCursorW( 0, IDC_ARROW );
			wc.hbrBackground = 0;
			wc.lpszMenuName = 0;
			wc.lpszClassName = VULWF_WIN32_WCNAME;
			if( !RegisterClassW( &wc ) )
				VULWF_THROW( "RegisterClass failed" );

			std::wstring const title_w = wstr_from_utf8( title_utf8 );
			hwnd_ = CreateWindowExW( cw_ex_style, VULWF_WIN32_WCNAME, title_w.c_str(), cw_style, 
				cw_x, cw_y, cw_width, cw_height, 0, 0, hinstance, reinterpret_cast<void *>( this ) );
			if( !hwnd_ )
				VULWF_THROW( "CreateWindowEx failed" );

			allow_dropfiles<t_logic>( hwnd_ );

			logic_.emplace( static_cast<window &>( *this ), std::forward<t_logic_args>( logic_args )... );

			if( fullscreen_ )
				wm_size( this, unsigned(cw_width), unsigned(cw_height) );

			ShowWindow( hwnd_, maximized_ ? SW_MAXIMIZE : SW_SHOW );
		}

		~window()
		{
			logic_.reset();
			DestroyWindow( hwnd_ );
			UnregisterClassW( VULWF_WIN32_WCNAME, GetModuleHandleW( nullptr ) );
		}

		t_logic & logic()
		{ return *logic_; }

		bool should_close() const
		{ return should_close_; }
		void should_close( bool should )
		{ should_close_ = should; }

		bool is_minimized() const
		{ return minimized_; }
		bool is_maximized() const
		{ return maximized_; }
		bool is_fullscreen() const
		{ return fullscreen_; }

		HWND os_handle() const
		{ return hwnd_; }

		static void poll_events()
		{
			MSG msg;
			while( PeekMessageW( &msg, nullptr, 0, 0, PM_REMOVE ) )
			{
				translate_message_if_needed<t_logic>( &msg );
				DispatchMessageW( &msg );
			}
		}

		static void wait_events()
		{
			WaitMessage();
			poll_events();
		}

		void toggle_fullscreen()
		{
			if( fullscreen_ )
			{
				SetWindowLongW( hwnd_, GWL_STYLE, WS_OVERLAPPEDWINDOW );
				SetWindowPos( hwnd_, HWND_NOTOPMOST, windowed_pos_.first, windowed_pos_.second, windowed_size_.first, windowed_size_.second,
					( maximized_ ? 0 : SWP_SHOWWINDOW ) | SWP_NOACTIVATE | SWP_NOCOPYBITS | SWP_FRAMECHANGED );
				if( maximized_ )
				{
					WINDOWPLACEMENT pl = {};
					pl.length = sizeof(WINDOWPLACEMENT);
					pl.showCmd = maximized_ ? SW_MAXIMIZE : SW_SHOW;
					pl.rcNormalPosition.left = windowed_pos_.first;
					pl.rcNormalPosition.right = windowed_pos_.first + windowed_size_.first;
					pl.rcNormalPosition.top = windowed_pos_.second;
					pl.rcNormalPosition.bottom = windowed_pos_.second + windowed_size_.second;
					SetWindowPlacement( hwnd_, &pl );
				}
				fullscreen_ = false;
			}
			else
			{
				fullscreen_ = true;

				RECT r;
				GetWindowRect( hwnd_, &r );
				int const x = (r.left + r.right)/2;
				int const y = (r.top + r.bottom)/2;
				auto const & monitors = get_monitors();
				monitor_info const * selected_monitor = nullptr;
				for( auto && im : monitors )
				{
					if( x >= im.x && y >= im.y && x < im.x + int(im.width) && y < im.y + int(im.height) )
					{
						selected_monitor = &im;
						break;
					}
				}
				if( !selected_monitor )
					selected_monitor = &monitors.front();

				fs_monitor_ = int( selected_monitor - &monitors.front() );

				WINDOWPLACEMENT pl;
				pl.length = sizeof(WINDOWPLACEMENT);
				if( maximized_ && GetWindowPlacement( hwnd_, &pl ) )
					r = pl.rcNormalPosition;

				windowed_pos_ = { r.left, r.top };
				windowed_size_ = { r.right - r.left, r.bottom - r.top };

				SetWindowLongW( hwnd_, GWL_STYLE, WS_POPUP );
				SetWindowPos( hwnd_, HWND_TOPMOST, selected_monitor->x, selected_monitor->y, selected_monitor->width, selected_monitor->height,
					SWP_SHOWWINDOW | SWP_NOACTIVATE | SWP_NOCOPYBITS | SWP_FRAMECHANGED );
			}
		}

		void set_title( char const * title_utf8 )
		{
			std::wstring const title_w = wstr_from_utf8( title_utf8 );
			SetWindowTextW( hwnd_, title_w.c_str() );
		}

		// params to recreate window with same windowed size and fullscreen status
		window_create_params window_recreate_params() const
		{
			RECT decor = { 0, 0, 0, 0 };
			AdjustWindowRectEx( &decor, WS_OVERLAPPEDWINDOW, FALSE, 0 );
			unsigned const decor_w = decor.right - decor.left;
			unsigned const decor_h = decor.bottom - decor.top;

			if( fullscreen_ )
				return { windowed_pos_, { windowed_size_.first - decor_w, windowed_size_.second - decor_h }, fs_monitor_ };
			
			WINDOWPLACEMENT pl;
			pl.length = sizeof(WINDOWPLACEMENT);
			GetWindowPlacement( hwnd_, &pl );

			return 
			{ 
				{ pl.rcNormalPosition.left, pl.rcNormalPosition.top }, 
				{ pl.rcNormalPosition.right - pl.rcNormalPosition.left - decor_w, pl.rcNormalPosition.bottom - pl.rcNormalPosition.top - decor_h }, 
				maximized_ ? create_maximized : create_windowed 
			};
		}

	private:
		HWND hwnd_;
		boost::optional<t_logic> logic_;
		bool should_close_;
		bool minimized_;
		bool maximized_;
		bool fullscreen_;
		std::pair<int,int> windowed_pos_;
		std::pair<unsigned,unsigned> windowed_size_;	// with decoration
		int fs_monitor_;

		static std::wstring wstr_from_utf8( char const * s )
		{
			int const length = MultiByteToWideChar( CP_UTF8, 0, s, -1, nullptr, 0 );
			std::wstring ws( size_t(length), L'\0' );
			if( MultiByteToWideChar( CP_UTF8, 0, s, -1, &ws[0], length ) == 0 )
				ws.clear();
			else
				ws.pop_back();
			return ws;
		}

		template<typename T>
		static auto wm_size( window<T> * this_, unsigned w, unsigned h ) -> decltype( std::declval<T>().resize( 0u, 0u ), void() )
		{ if( !this_->minimized_ && this_->logic_ ) this_->logic_->resize( w, h ); }
		template<typename T>
		static void wm_size( window<T> *, ... ) {}

		template<typename T>
		static auto wm_getminmaxinfo( UINT message, LPARAM lparam )
			-> decltype( T::min_view_width, T::min_view_height, false )
		{
			if( message == WM_GETMINMAXINFO )
			{
				MINMAXINFO * info = reinterpret_cast<MINMAXINFO *>(lparam);
				RECT r = { 0, 0, T::min_view_width, T::min_view_height };
				AdjustWindowRectEx( &r, WS_OVERLAPPEDWINDOW, FALSE, 0 );
				info->ptMinTrackSize.x = r.right - r.left;
				info->ptMinTrackSize.y = r.bottom - r.top;
				return true;
			}
			return false;
		}
		template<typename T>
		static bool wm_getminmaxinfo( ... )
		{
			return false;
		}

		template<typename T>
		static auto wm_char( window<T> * this_, UINT message, WPARAM wparam )
			-> decltype( std::declval<T>().input_char( L'\0' ), false )
		{
			if( message == WM_CHAR )
			{
				this_->logic_->input_char( wchar_t( wparam ) );
				return true;
			}
			return false;
		}
		template<typename T>
		static auto translate_message_if_needed( MSG * msg ) -> decltype( std::declval<T>().input_char( L'\0' ), void() )
		{ TranslateMessage( msg ); }
		template<typename T>
		static bool wm_char( window<T> *, ... ) { return false; }
		template<typename T>
		static void translate_message_if_needed( ... ) {}

		template<typename T>
		static auto wm_mousemove( window<T> * this_, UINT message, LPARAM lparam )
			-> decltype( std::declval<T>().mouse_move( 0, 0 ), false )
		{
			if( message == WM_MOUSEMOVE )
			{
				this_->logic_->mouse_move( LOWORD( lparam ), HIWORD( lparam ) );
				return true;
			}
			return false;
		}
		template<typename T>
		static bool wm_mousemove( window<T> *, ... ) { return false; }

		template<typename T>
		static auto wm_mousebutton( window<T> * this_, UINT message )
			-> decltype( std::declval<T>().mouse_button( 0, false ), false )
		{
			switch( message )
			{
			case WM_LBUTTONDOWN: this_->logic_->mouse_button( 0, true ); return true;
			case WM_LBUTTONUP: this_->logic_->mouse_button( 0, false ); return true;
			case WM_MBUTTONDOWN: this_->logic_->mouse_button( 1, true ); return true;
			case WM_MBUTTONUP: this_->logic_->mouse_button( 1, false ); return true;
			case WM_RBUTTONDOWN: this_->logic_->mouse_button( 2, true ); return true;
			case WM_RBUTTONUP: this_->logic_->mouse_button( 2, false ); return true;
			}
			return false;
		}
		template<typename T>
		static bool wm_mousebutton( window<T> *, ... ) { return false; }

		template<typename T>
		static auto wm_mousewheel( window<T> * this_, UINT message, WPARAM wparam )
			-> decltype( std::declval<T>().mouse_wheel( 0 ), false )
		{
			if( message == WM_MOUSEWHEEL )
			{
				this_->logic_->mouse_wheel( GET_WHEEL_DELTA_WPARAM( wparam ) );
				return true;
			}
			return false;
		}
		template<typename T>
		static bool wm_mousewheel( window<T> *, ... ) { return false; }

		template<typename T>
		static auto ok_to_close( window<T> * this_ ) -> decltype( std::declval<T>().ok_to_close(), false )
		{ return this_->logic_->ok_to_close(); }
		template<typename T>
		static bool ok_to_close( ... ) { return true; }

		template<typename T>
		static auto wm_kbd( window<T> * this_, UINT message, WPARAM wparam, LPARAM lparam )
			-> decltype( std::declval<T>().key( 0, false ), false )
		{
			switch( message )
			{
			case WM_KEYDOWN: 
				if( !( lparam & (1<<30) ) ) // skip auto-repeat
					this_->logic_->key( unsigned(wparam), true ); 
				return true;
			case WM_KEYUP: 
				this_->logic_->key( unsigned(wparam), false ); 
				return true;
			case WM_SYSKEYDOWN:
				if( wparam == VK_F4 && lparam&(1<<29) )
					break; // process alt-f4
				if( !( lparam & (1<<30) ) ) // skip auto-repeat
					this_->logic_->key( unsigned(wparam), true ); 
				return true;
			case WM_SYSKEYUP:
				if( wparam == VK_F4 && lparam&(1<<29) )
					break; // process alt-f4
				this_->logic_->key( unsigned(wparam), false ); 
				return true;
			}
			return false;
		}
		template<typename T>
		static bool wm_kbd( window<T> *, ... ) { return false; }

		template<typename T>
		static auto allow_dropfiles( HWND hwnd ) -> decltype( std::declval<T>().drop_files( {} ), void() )
		{ DragAcceptFiles( hwnd, TRUE ); }
		template<typename T>
		static auto wm_dropfiles( window<T> * this_, UINT message, WPARAM wparam )
			-> decltype( std::declval<T>().drop_files( {} ), false )
		{
			if( message == WM_DROPFILES )
			{
				HDROP const drop = (HDROP)wparam;
				unsigned const num_files = DragQueryFileA( drop, unsigned( -1 ), nullptr, 0 );
				std::vector<std::string> filenames( num_files );
				for( unsigned i = 0; i != num_files; ++i )
				{
					unsigned const filename_size = DragQueryFileA( drop, i, nullptr, 0 );
					std::string & filename = filenames[i];
					filename.resize( filename_size + 1 );
					DragQueryFileA( drop, i, &filename[0], filename_size + 1 );
					filename.pop_back();
				}
				this_->logic_->drop_files( filenames );
				DragFinish( drop );
				return true;
			}
			return false;
		}
		template<typename T>
		static void allow_dropfiles( ... ) {}
		template<typename T>
		static bool wm_dropfiles( window<T> *, ... ) { return false; }

		static LRESULT CALLBACK wndproc( HWND hwnd, UINT message, WPARAM wparam, LPARAM lparam )
		{
			auto this_ = reinterpret_cast<window *>( GetWindowLongPtr( hwnd, GWLP_USERDATA ) );

			if( wm_mousemove( this_, message, lparam ) 
				|| wm_mousebutton( this_, message ) 
				|| wm_mousewheel( this_, message, wparam ) 
				|| wm_kbd( this_, message, wparam, lparam ) 
				|| wm_char( this_, message, wparam )
				|| wm_getminmaxinfo<t_logic>( message, lparam )
				|| wm_dropfiles( this_, message, wparam ) )
				return 0;

			switch( message )
			{
			case WM_SIZE:
				this_->minimized_ = wparam == SIZE_MINIMIZED; 
				if( !this_->fullscreen_ ) this_->maximized_ = wparam == SIZE_MAXIMIZED;
				wm_size( this_, LOWORD( lparam ), HIWORD( lparam ) );
				return 0;
			case WM_CREATE:
				this_ = reinterpret_cast<window *>( reinterpret_cast<CREATESTRUCT *>( lparam )->lpCreateParams );
				assert( this_ );
				SetWindowLongPtr( hwnd, GWLP_USERDATA, reinterpret_cast<LONG_PTR>( this_ ) );
				return 0;
			case WM_CLOSE:
				if( ok_to_close<t_logic>( this_ ) )
				{
					this_->should_close_ = true;
					this_->minimized_ = true;
				}
				return 0;
			}

			return DefWindowProc( hwnd, message, wparam, lparam );
		}
	};
}
