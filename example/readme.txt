I'm trying to find a good way to use swapchain for CAD-like scenarios. Rendering is performed not in a loop frame after frame but only if something is changed.

I create VK_PRESENT_MODE_FIFO_KHR swapchain with 2 images, basic vsync mode.

In this demo app frame is re-rendered on each left mouse button click. Each time with a differet color, color and swapchin image are written into console.

And on my NVIDIA GT 740M with 368.22 drivers on Win10 x64 I have 2 problems with this code:

1. Only rendering into swapchain image #0 is visible. Looks like vkQueuePresentKHR with image #1 is ignored by driver.

2. If window is left without updates for some time (around 30 seconds) index buffer (in device local memory) gets corrupted http://rusteddreams.bitbucket.org/vulkan/ibcorrupt.png

Pre-built binary can be downloaded here: http://rusteddreams.bitbucket.org/vulkan/bin.zip