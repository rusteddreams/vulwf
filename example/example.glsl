#pragma stage_vert

#version 450

#extension GL_ARB_separate_shader_objects : enable
#extension GL_ARB_shading_language_420pack : enable

layout( location = 0 ) in vec4 P;

layout( push_constant ) uniform u_push_vert
{
	mat4 m;
} U;

out gl_PerVertex
{
	vec4 gl_Position;
};

layout( location = 0 ) out float l;

void main()
{
	gl_Position = U.m * vec4( P.xyz, 1 );
	l = max(0.,P.z);
}

#pragma stage_frag

#version 450

#extension GL_ARB_separate_shader_objects : enable
#extension GL_ARB_shading_language_420pack : enable

layout( location = 0 ) in float l;

layout( push_constant ) uniform u_push_frag
{
	layout( offset = 64 ) vec3 c;
} U;

layout( location = 0, index = 0 ) out vec4 F;

void main()
{
	F = vec4( U.c*l, 1 );
}
