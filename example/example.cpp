#include <ctime>
#include <iomanip>
#include <iostream>
#include <boost/exception/diagnostic_information.hpp>

#define VULWF_IMPLEMENTATION
#include <vulwf.h>

#include "example.glsl.hpp"

namespace detail
{
	template<unsigned bits, typename T>
	constexpr uint32_t inorm( T x )
	{ return uint32_t( int32_t( x*((1<<(bits-1))-1) + (x > 0 ? T( 0.5 ) : T( -0.5 )) ) )&((1<<bits)-1); }
}
template<typename T>
constexpr uint32_t pack_inorm1010102( T x, T y, T z, T w )
{ return detail::inorm<10>(x) | (detail::inorm<10>(y)<<10) | (detail::inorm<10>(z)<<20) | (detail::inorm<2>(w)<<30); }

uint32_t const g_geom_grid_size = 128;

std::pair<vulwf::uh<VkBuffer>, vulwf::uh<VkDeviceMemory>> create_vb( VkCommandBuffer cmdbuf_setup )
{
	VkFormatProperties fmt_props;
	vkGetPhysicalDeviceFormatProperties( vulwf::single_vk_device::phys_device_, VK_FORMAT_A2B10G10R10_SNORM_PACK32, &fmt_props );
	if( !(fmt_props.bufferFeatures&VK_FORMAT_FEATURE_VERTEX_BUFFER_BIT) )
		VULWF_THROW( "vertex format A2B10G10R10_SNORM_PACK32 unavailable" );

	float const w0 = -2.f;
	float const w1 = 2.f;
	float const scale = (w1-w0)/(g_geom_grid_size-1);
	uint32_t vertices[g_geom_grid_size*g_geom_grid_size];

	uint32_t * v = vertices;
	for( uint32_t iy = 0; iy != g_geom_grid_size; ++iy )
	{
		float const y = scale*iy + w0;
		float const cy = cos(y);
		for( uint32_t ix = 0; ix != g_geom_grid_size; ++ix )
		{
			float const x = scale*ix + w0;
			float const z = cos(x)+cy;
			*v++ = pack_inorm1010102( x/2, y/2, z/2, 1.f );
		}
	}

	return vulwf::create_static_buffer( vertices, sizeof(vertices), 
		VK_BUFFER_USAGE_VERTEX_BUFFER_BIT, cmdbuf_setup, vulwf::single_vk_device::queue_ );
}

std::pair<vulwf::uh<VkBuffer>, vulwf::uh<VkDeviceMemory>> create_ib( VkCommandBuffer cmdbuf_setup )
{
	uint16_t indices[3*2*(g_geom_grid_size-1)*(g_geom_grid_size-1)];

	uint16_t * i = indices;
	for( uint32_t iy = 0; iy != g_geom_grid_size-1; ++iy )
	{
		auto const row = iy*g_geom_grid_size;
		for( uint32_t ix = 0; ix != g_geom_grid_size-1; ++ix )
		{
			*i++ = uint16_t( row + ix );
			*i++ = uint16_t( row + ix + 1 );
			*i++ = uint16_t( row + g_geom_grid_size + ix );
			*i++ = uint16_t( row + g_geom_grid_size + ix );
			*i++ = uint16_t( row + ix + 1 );
			*i++ = uint16_t( row + g_geom_grid_size + ix + 1 );
		}
	}

	return vulwf::create_static_buffer( indices, sizeof(indices), 
		VK_BUFFER_USAGE_INDEX_BUFFER_BIT, cmdbuf_setup, vulwf::single_vk_device::queue_ );
}

vulwf::uh<VkRenderPass> create_render_pass( VkFormat color_format, bool clear_color = true, VkFormat depth_fromat = VK_FORMAT_D16_UNORM )
{
	VkAttachmentDescription attachments[2] = {};

	attachments[0].format = color_format;
	attachments[0].samples = VK_SAMPLE_COUNT_1_BIT;
	attachments[0].loadOp = clear_color ? VK_ATTACHMENT_LOAD_OP_CLEAR : VK_ATTACHMENT_LOAD_OP_DONT_CARE;
	attachments[0].storeOp = VK_ATTACHMENT_STORE_OP_STORE;
	attachments[0].stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
	attachments[0].stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
	attachments[0].initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
	attachments[0].finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;

	attachments[1].format = depth_fromat;
	attachments[1].samples = VK_SAMPLE_COUNT_1_BIT;
	attachments[1].loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
	attachments[1].storeOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
	attachments[1].stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
	attachments[1].stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
	attachments[1].initialLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;
	attachments[1].finalLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;

	VkAttachmentReference color_attach_ref = {};
	color_attach_ref.attachment = 0;
	color_attach_ref.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

	VkAttachmentReference depth_attach_ref = {};
	depth_attach_ref.attachment = 1;
	depth_attach_ref.layout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;

	VkSubpassDescription subpasses[1] = {};
	subpasses[0].pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;
	subpasses[0].colorAttachmentCount = 1;
	subpasses[0].pColorAttachments = &color_attach_ref;
	subpasses[0].pDepthStencilAttachment = &depth_attach_ref;

	VkSubpassDependency const dependencies[] = 
	{ 
		{
			VK_SUBPASS_EXTERNAL, 0,
			VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT, VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT,
			0, VK_ACCESS_COLOR_ATTACHMENT_READ_BIT,
			0
		},
	};

	VkRenderPassCreateInfo render_pass_info = { VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO };
	render_pass_info.attachmentCount = 2;
	render_pass_info.pAttachments = attachments;
	render_pass_info.subpassCount = sizeof(subpasses)/sizeof(subpasses[0]);
	render_pass_info.pSubpasses = subpasses;
	render_pass_info.dependencyCount = sizeof(dependencies)/sizeof(dependencies[0]);
	render_pass_info.pDependencies = dependencies;

	vulwf::uh<VkRenderPass> rpass;
	if( vkCreateRenderPass( vulwf::single_vk_device::device_, &render_pass_info, nullptr, rpass.init_ptr() ) != VK_SUCCESS )
		VULWF_THROW( "vkCreateRenderPass failed" );
	return rpass;
}

vulwf::uh<VkPipelineLayout> create_pipeline_layout()
{
	VkPushConstantRange const push_ranges[] = 
	{
		{ VK_SHADER_STAGE_VERTEX_BIT, 0, sizeof(float)*16 },
		{ VK_SHADER_STAGE_FRAGMENT_BIT, sizeof(float)*16, sizeof(float)*3 }
	};

	VkPipelineLayoutCreateInfo layout_info = { VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO };
	layout_info.setLayoutCount = 0;
	layout_info.pSetLayouts = nullptr;
	layout_info.pushConstantRangeCount = sizeof(push_ranges)/sizeof(push_ranges[0]);
	layout_info.pPushConstantRanges = push_ranges;

	vulwf::uh<VkPipelineLayout> pl;
	if( vkCreatePipelineLayout( vulwf::single_vk_device::device_, &layout_info, nullptr, pl.init_ptr() ) != VK_SUCCESS )
		VULWF_THROW( "vkCreatePipelineLayout failed" );
	return pl;
}

vulwf::uh<VkPipeline> create_pipeline( VkPipelineLayout layout, VkRenderPass render_pass )
{
	auto vshader = vulwf::create_vk_shader_module( example_vert, sizeof(example_vert) );
	auto fshader = vulwf::create_vk_shader_module( example_frag, sizeof(example_frag) );
	VkPipelineShaderStageCreateInfo shader_info[2] = { { VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO }, { VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO } };
	shader_info[0].stage = VK_SHADER_STAGE_VERTEX_BIT;
	shader_info[0].module = vshader;
	shader_info[0].pName = "main";
	shader_info[1].stage = VK_SHADER_STAGE_FRAGMENT_BIT;
	shader_info[1].module = fshader;
	shader_info[1].pName = "main";

	VkVertexInputBindingDescription vertex_binding_desc = {};
	vertex_binding_desc.binding = 0;
	vertex_binding_desc.stride = sizeof(uint32_t);
	vertex_binding_desc.inputRate = VK_VERTEX_INPUT_RATE_VERTEX;

	VkVertexInputAttributeDescription const vertex_attrib_desc[] = 
	{
		{ 0, 0, VK_FORMAT_A2B10G10R10_SNORM_PACK32, 0 },
	};

	VkPipelineVertexInputStateCreateInfo vertex_input_info = { VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO };
	vertex_input_info.vertexBindingDescriptionCount = 1;
	vertex_input_info.pVertexBindingDescriptions = &vertex_binding_desc;
	vertex_input_info.vertexAttributeDescriptionCount = sizeof(vertex_attrib_desc)/sizeof(vertex_attrib_desc[0]);
	vertex_input_info.pVertexAttributeDescriptions = vertex_attrib_desc;

	VkPipelineInputAssemblyStateCreateInfo input_assembly_info = { VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO };
	input_assembly_info.topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;
	input_assembly_info.primitiveRestartEnable = VK_FALSE;

	VkPipelineViewportStateCreateInfo viewport_info = { VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO };
	viewport_info.viewportCount = 1;
	viewport_info.pViewports = nullptr;
	viewport_info.scissorCount = 1;
	viewport_info.pScissors = nullptr;

	VkPipelineRasterizationStateCreateInfo rasterization_info = { VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO };
	rasterization_info.depthClampEnable = VK_FALSE;
	rasterization_info.rasterizerDiscardEnable = VK_FALSE;
	rasterization_info.polygonMode = VK_POLYGON_MODE_FILL;
	rasterization_info.cullMode = VK_CULL_MODE_BACK_BIT;
	rasterization_info.frontFace = VK_FRONT_FACE_COUNTER_CLOCKWISE;
	rasterization_info.depthBiasEnable = VK_FALSE;
	rasterization_info.depthBiasConstantFactor = 0;
	rasterization_info.depthBiasClamp = 0;
	rasterization_info.depthBiasSlopeFactor = 0;
	rasterization_info.lineWidth = 1;

	VkPipelineMultisampleStateCreateInfo multisample_info = { VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO };
	multisample_info.rasterizationSamples = VK_SAMPLE_COUNT_1_BIT;
	multisample_info.sampleShadingEnable = VK_FALSE;
	multisample_info.minSampleShading = 0;
	multisample_info.pSampleMask = nullptr;
	multisample_info.alphaToCoverageEnable = VK_FALSE;
	multisample_info.alphaToOneEnable = VK_FALSE;

	VkStencilOpState nop_stencil = {};
	nop_stencil.failOp = VK_STENCIL_OP_KEEP;
	nop_stencil.passOp = VK_STENCIL_OP_KEEP;
	nop_stencil.depthFailOp = VK_STENCIL_OP_KEEP;
	nop_stencil.compareOp = VK_COMPARE_OP_ALWAYS;
	nop_stencil.compareMask = 0;
	nop_stencil.writeMask = 0;
	nop_stencil.reference = 0;

	VkPipelineDepthStencilStateCreateInfo depth_info = { VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO };
	depth_info.depthTestEnable = VK_TRUE;
	depth_info.depthWriteEnable = VK_TRUE;
	depth_info.depthCompareOp = VK_COMPARE_OP_LESS_OR_EQUAL;
	depth_info.depthBoundsTestEnable = VK_FALSE;
	depth_info.stencilTestEnable = VK_FALSE;
	depth_info.front = nop_stencil;
	depth_info.back = nop_stencil;
	depth_info.minDepthBounds = 0;
	depth_info.maxDepthBounds = 0;

	VkPipelineColorBlendAttachmentState color_blend_attachment = {};
	color_blend_attachment.blendEnable = VK_FALSE;
	color_blend_attachment.srcColorBlendFactor = VK_BLEND_FACTOR_SRC_COLOR;
	color_blend_attachment.dstColorBlendFactor = VK_BLEND_FACTOR_ONE_MINUS_DST_COLOR;
	color_blend_attachment.colorBlendOp = VK_BLEND_OP_ADD;
	color_blend_attachment.srcAlphaBlendFactor = VK_BLEND_FACTOR_ZERO;
	color_blend_attachment.dstAlphaBlendFactor = VK_BLEND_FACTOR_ZERO;
	color_blend_attachment.alphaBlendOp = VK_BLEND_OP_ADD;
	color_blend_attachment.colorWriteMask = VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT;

	VkPipelineColorBlendStateCreateInfo blend_info = { VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO };
	blend_info.logicOpEnable = VK_FALSE;
	blend_info.logicOp = VK_LOGIC_OP_CLEAR;
	blend_info.attachmentCount = 1;
	blend_info.pAttachments = &color_blend_attachment;
	blend_info.blendConstants[0] = 0.0;
	blend_info.blendConstants[1] = 0.0;
	blend_info.blendConstants[2] = 0.0;
	blend_info.blendConstants[3] = 0.0;

	VkDynamicState const dynamic_states[2] = { VK_DYNAMIC_STATE_VIEWPORT, VK_DYNAMIC_STATE_SCISSOR };
	VkPipelineDynamicStateCreateInfo dynamic_info = { VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO };
	dynamic_info.dynamicStateCount = sizeof(dynamic_states)/sizeof(dynamic_states[0]);
	dynamic_info.pDynamicStates = dynamic_states;

	VkGraphicsPipelineCreateInfo pipeline_info = { VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO };
	pipeline_info.stageCount = sizeof(shader_info)/sizeof(shader_info[0]);
	pipeline_info.pStages = shader_info;
	pipeline_info.pVertexInputState = &vertex_input_info;
	pipeline_info.pInputAssemblyState = &input_assembly_info;
	pipeline_info.pTessellationState = nullptr;
	pipeline_info.pViewportState = &viewport_info;
	pipeline_info.pRasterizationState = &rasterization_info;
	pipeline_info.pMultisampleState = &multisample_info;
	pipeline_info.pDepthStencilState = &depth_info;
	pipeline_info.pColorBlendState = &blend_info;
	pipeline_info.pDynamicState = &dynamic_info;
	pipeline_info.layout = layout;
	pipeline_info.renderPass = render_pass;
	pipeline_info.subpass = 0;

	vulwf::uh<VkPipeline> p;
	if( vkCreateGraphicsPipelines( vulwf::single_vk_device::device_, nullptr, 1, &pipeline_info, nullptr, p.init_ptr() ) != VK_SUCCESS )
		VULWF_THROW( "vkCreateGraphicsPipelines failed" );

	return p;
}

struct example_logic
{
	example_logic( vulwf::window<example_logic> & w )
		: w_( w ), 
		module_(),
		instance_( module_, "vulwf example" ),
#ifdef VULWF_USE_DEBUG_REPORTING
		debuglog_( vulwf::create_vk_debuglog( 
			[]( VkDebugReportFlagsEXT /*flags*/, VkDebugReportObjectTypeEXT /*obj_type*/, uint64_t /*object*/,
				size_t /*location*/, int32_t /*msg_code*/, char const * /*layer_prefix*/, char const * msg, void * /*user_data*/ ) -> VkBool32
	{
		std::cerr << msg << '\n';
		return VK_FALSE;
	} ) ),
#endif
		surface_( vulwf::create_vk_surface( w.os_handle() ) ),
		device_( surface_ ),
		cmdpool_( vulwf::create_vk_cmdpool( VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT ) ),
		cmdbuf_setup_( vulwf::create_vk_cmdbuf( cmdpool_ ) ),
		cmdbuf_draw_( vulwf::create_vk_cmdbuf( cmdpool_ ) ),
		fence_( vulwf::create_vk_fence() ),
		swapchain_( surface_ ),
		depth_(),
		render_pass_( create_render_pass( swapchain_.selected_format_.format ) ),
		framebuffers_(),
		vb_( create_vb( cmdbuf_setup_ ) ),
		ib_( create_ib( cmdbuf_setup_ ) ),
		pp_layout_( create_pipeline_layout() ),
		pipeline_( create_pipeline( pp_layout_, render_pass_ ) )
	{
		char const * dev_type = "<unknown>";
		switch( vulwf::single_vk_device::phys_device_props_.deviceType )
		{
		case VK_PHYSICAL_DEVICE_TYPE_OTHER: dev_type = "other"; break;
		case VK_PHYSICAL_DEVICE_TYPE_INTEGRATED_GPU: dev_type = "integrated GPU"; break;
		case VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU: dev_type = "discrete GPU"; break;
		case VK_PHYSICAL_DEVICE_TYPE_VIRTUAL_GPU: dev_type = "virtual GPU"; break;
		case VK_PHYSICAL_DEVICE_TYPE_CPU: dev_type = "CPU"; break;
		}

		auto api_ver = vulwf::single_vk_device::phys_device_props_.apiVersion;
		std::cout << "Running on:\n"
			"\tVulkan API version: " << (api_ver>>22) << '.' << ((api_ver>>12)&1023) << '.' << (api_ver&4095) << "\n"
			"\tDevice: " << vulwf::single_vk_device::phys_device_props_.deviceName << " (" 
			<< std::hex << vulwf::single_vk_device::phys_device_props_.vendorID << '-'
			<< std::hex << vulwf::single_vk_device::phys_device_props_.deviceID << "), " << dev_type << "\n"
			"\tDriver version: " << std::dec << vulwf::single_vk_device::phys_device_props_.driverVersion << '\n';
	}

	void resize( unsigned width, unsigned height )
	{
		view_size_ = { width, height };
		vkDeviceWaitIdle( vulwf::single_vk_device::device_ );
		swapchain_.recreate( surface_, view_size_ );
		depth_.recreate( cmdbuf_setup_, view_size_ );
		framebuffers_ = vulwf::create_swap_frambuffers( depth_.depth_view_, render_pass_, 
			swapchain_.image_views_, swapchain_.images_count_, view_size_ );

		dirty_ = true;
	}

	void mouse_button( unsigned b, bool pressed )
	{
		if( b == 0 && pressed )
			dirty_ = true;
	}

	void update()
	{
		if( !dirty_ )
			return;

		dirty_ = false;

		vulwf::uh<VkSemaphore> present_complete, rendering_complete;
		VkSemaphoreCreateInfo const semaphore_info = { VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO, 0, 0 };
		vkCreateSemaphore( vulwf::single_vk_device::device_, &semaphore_info, nullptr, present_complete.init_ptr() );
		vkCreateSemaphore( vulwf::single_vk_device::device_, &semaphore_info, nullptr, rendering_complete.init_ptr() );

		uint32_t next_image;
		vkAcquireNextImageKHR( vulwf::single_vk_device::device_, swapchain_.swapchain_, UINT64_MAX, present_complete, fence_, &next_image );

		vkWaitForFences( vulwf::single_vk_device::device_, 1, fence_.addr(), VK_TRUE, UINT64_MAX );
		vkResetFences( vulwf::single_vk_device::device_, 1, fence_.addr() );

		vkResetCommandBuffer( cmdbuf_draw_, 0 );

		static float const colors[][3] = 
		{
			{ 1, 0, 0 }, { 0, 1, 0 }, { 0, 0, 1 }, { 1, 1, 1 }, { 1, 1, 0 }
		};
		static int clr_idx = 0;
		float const * clr = colors[clr_idx];
		clr_idx = (clr_idx+1)%(sizeof(colors)/sizeof(colors[0]));
		std::cout << "Rendering to image " << next_image << " with (" << clr[0] << ' ' << clr[1] << ' ' << clr[2] << ") color\n";
 		record_draw_buf( cmdbuf_draw_, next_image, clr );

		VkPipelineStageFlags wait_stage_mask = { VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT };
		VkSubmitInfo submit_info = { VK_STRUCTURE_TYPE_SUBMIT_INFO };
		submit_info.waitSemaphoreCount = 1;
		submit_info.pWaitSemaphores = present_complete.addr();
		submit_info.pWaitDstStageMask = &wait_stage_mask;
		submit_info.commandBufferCount = 1;
		submit_info.pCommandBuffers = &cmdbuf_draw_;
		submit_info.signalSemaphoreCount = 1;
		submit_info.pSignalSemaphores = rendering_complete.addr();
		vkQueueSubmit( vulwf::single_vk_device::queue_, 1, &submit_info, VK_NULL_HANDLE );

		VkPresentInfoKHR present_info = { VK_STRUCTURE_TYPE_PRESENT_INFO_KHR };
		present_info.waitSemaphoreCount = 1;
		present_info.pWaitSemaphores = rendering_complete.addr();
		present_info.swapchainCount = 1;
		present_info.pSwapchains = swapchain_.swapchain_.addr();
		present_info.pImageIndices = &next_image;
		present_info.pResults = nullptr;
		vkQueuePresentKHR( vulwf::single_vk_device::queue_, &present_info );

		vkQueueWaitIdle( vulwf::single_vk_device::queue_ );
	}
private:
	vulwf::window<example_logic> & w_;
	std::pair<unsigned,unsigned> view_size_;

	vulwf::vulkan_module module_;
	vulwf::vk_instance instance_;
#ifdef VULWF_USE_DEBUG_REPORTING
	vulwf::uh<VkDebugReportCallbackEXT> debuglog_;
#endif
	vulwf::uh<VkSurfaceKHR> surface_;
	vulwf::single_vk_device device_;
	vulwf::uh<VkCommandPool> cmdpool_;
	VkCommandBuffer cmdbuf_setup_;
	VkCommandBuffer cmdbuf_draw_;
	vulwf::uh<VkFence> fence_;
	vulwf::vulkan_swapchain swapchain_;
	vulwf::vulkan_depthbuf depth_;
	vulwf::uh<VkRenderPass> render_pass_;
	std::array<vulwf::uh<VkFramebuffer>, VULWF_MAX_SWAP_IMAGES> framebuffers_;
	std::pair<vulwf::uh<VkBuffer>, vulwf::uh<VkDeviceMemory>> vb_, ib_;
	vulwf::uh<VkPipelineLayout> pp_layout_;
	vulwf::uh<VkPipeline> pipeline_;
	bool dirty_ = true;

	void record_draw_buf( VkCommandBuffer buf, uint32_t image_index, float const * color )
	{
		VkCommandBufferBeginInfo begin_info = { VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO };
		begin_info.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;

		vkBeginCommandBuffer( buf, &begin_info );

		VkClearValue const clear_values[] = { { 0.5f, 0.5f, 0.5f, 1.0f }, { 1.0, 0.0 } };
		VkRenderPassBeginInfo rpass_info = { VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO };
		rpass_info.renderPass = render_pass_;
		rpass_info.framebuffer = framebuffers_[image_index];
		rpass_info.renderArea = { 0, 0, view_size_.first, view_size_.second };
		rpass_info.clearValueCount = 2;
		rpass_info.pClearValues = clear_values;

		vkCmdBeginRenderPass( buf, &rpass_info, VK_SUBPASS_CONTENTS_INLINE );

		VkDeviceSize const offset = 0;
		vkCmdBindVertexBuffers( buf, 0, 1, vb_.first.addr(), &offset );
		vkCmdBindIndexBuffer( buf, ib_.first, 0, VK_INDEX_TYPE_UINT16 );

		vkCmdBindPipeline( buf, VK_PIPELINE_BIND_POINT_GRAPHICS, pipeline_ );    

		VkViewport const viewport = { 0, 0, float(view_size_.first), float(view_size_.second), 0, 1 };
		vkCmdSetViewport( buf, 0, 1, &viewport );

		VkRect2D const scissor = { 0, 0, view_size_.first, view_size_.second };
		vkCmdSetScissor( buf, 0, 1, &scissor );

		float const mvp[16] = 
		{
			-1.22474480f, 1.15470052f, -0.508474469f, -0.499999911f,
			1.22474468f, 1.15470040f, -0.508474529f, -0.499999940f,
			0.000000000f, -1.63299298f, -0.719091713f, -0.707106829f,
			-0.000000000f, 0.000000000f, 4.57627106f, 5.00000000f
		};

		vkCmdPushConstants( buf, pp_layout_, VK_SHADER_STAGE_VERTEX_BIT, 0, sizeof(mvp), mvp );
		vkCmdPushConstants( buf, pp_layout_, VK_SHADER_STAGE_FRAGMENT_BIT, sizeof(mvp), sizeof(float)*3, color );
		vkCmdDrawIndexed( buf, 3*2*(g_geom_grid_size-1)*(g_geom_grid_size-1), 1, 0, 0, 0 );

		vkCmdEndRenderPass( buf );
		vkEndCommandBuffer( buf );
	}
};

int main()
{
	try
	{
		vulwf::window<example_logic> w( u8"example", { {20,20}, {512,384}, vulwf::create_windowed } );

		while( !w.should_close() )
		{
			w.wait_events();
			w.logic().update();
		}
	}
	catch( ... )
	{
		auto const & ex_info = boost::current_exception_diagnostic_information();
		std::cerr << ex_info;
		return 1;
	}
	return 0;
}
